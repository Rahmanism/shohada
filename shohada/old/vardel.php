<html dir="rtl">
<?php
@session_start();
require_once('inc.php');
html_top('','حذف');

$vardel_submit = $_POST['vardel_submit'];
$cc = $_REQUEST['cc'];
if (($cc <> 2) and ($cc <> 1))
  $cc = $_POST['cc'];

$table = $_POST['tablename'];
if (!$table)
  $table = $_REQUEST['tablename'];
$spage = $_POST['spage'];
if (!$spage)
  $spage = $_REQUEST['spage'];
$d_id = $_REQUEST['id'];
$d_ids = $_POST['ids'];
$m = 0;

if ($d_id)
{
  if ($vardel_submit)
    $m += delete_var($table, $d_id);
  else
    delete_confirm($table, $d_id, $cc, $spage);
}
else
{
  if ($vardel_submit)
  {
    $d_ids = $_SESSION['ids'];
    foreach($d_ids as $index=>$on)
      $m += delete_var($table, $index);
  }
  else
    delete_confirm_ar($table, $d_ids, $cc, $spage);
}

if ($vardel_submit)
{
  if (!$m)
    $mymsg = 'ركوردي با اين شماره پيدا نشد و يا قابل حذف نبود.';
  else
    $mymsg = $m.' ركورد حذف شد.';

  redirect_rel($spage.'?cc='.$cc, $mymsg, 1);

  unset($_SESSION['ids']);
//  session_destroy();
}

html_bottom();
?>
