﻿<?php
//************************** MYDBCONNECT
function mydbconnect()
{
  global $main_url;
  global $hostname_shohada;
  global $database_shohada;
  global $username_shohada;
  global $password_shohada;
  global $shohadaconn;
  
  $shohadaconn = @mysql_pconnect($hostname_shohada, $username_shohada, $password_shohada);
  if (!$shohadaconn)
    die('خطا در اتصال به سرور بانک اطلاعاتی');
  if (!@mysql_select_db($database_shohada))
    die('خطا در برقراری ارتباط با بانک اطلاعاتی');
}
//************************** MYDBCONNECT - end

//************************** GET CATEGORIES
function get_categories($ml = 'm')//$pid = 0)
{
  $ccodes = 'select id, title from '.$ml.'categories';// a where a.pid='.$pid;
  $ccodes = mysql_query($ccodes);
  $ccno = mysql_num_rows($ccodes);
  return array($ccno, $ccodes);
}
//************************** GET CATEGORIES - end

//************************** GET CATEGORY
function get_category($id = 0, $ml = 'm')
{
  $a_category = 'select id, title from '.$ml.'categories a where a.id='.$id;
  $a_category = mysql_query($a_category);
  $a_n = mysql_num_rows($a_category);
  if (!$a_n)
    return 0;
  else
  {
    $a_category = mysql_fetch_object($a_category);
    return $a_category->title;
  }
}
//************************** GET CATEGORY - end

//************************** GET CHILD CATEGORIES
function get_child_categories($pid = 0, $e_pid = 0, $new = 1)
{
  $child_categories = 'select id, title from mcategories a where a.pid='.$pid;
  $child_categories = mysql_query($child_categories);
  $children_no = mysql_num_rows($child_categories);
  if ($children_no > 0)
  {
    global $spaces;
    $spaces .= '&nbsp;';
    for ($i = 0; $i < $children_no; $i++)
    {
      $child_category = mysql_fetch_object($child_categories);
      if (!$new)
      {
            if ($cc1->id == $e_pid)
            $selected = ' selected';
          else
            $selected = '';
      }
        echo '<option value="'.$child_category->id.'"'.$selected.'>'.
        $spaces.$child_category->name.'</option>'."\n";
        get_child_categories($child_category->id, $e_pid, 0);
    }
    $spaces = substr($spaces, 0, strlen($spaces) - 6);
  }
  return;
}
//************************** GET CHILD CATEGORIES - end

//************************** GET TABLE ID
function get_table_id($name)
{
  $query = 'select id from stables where name="'.$name.'"';
  $query = mysql_query($query);
  $query = mysql_fetch_object($query);
  return $query->id;
}
//************************** GET TABLE ID - end

//************************** GET TABLE ID
function get_table_name($id)
{
  $query = 'select name from stables a where a.id='.$id;
  $query = mysql_query($query);
  $query = mysql_fetch_object($query);
  return $query->name;
}
//************************** GET TABLE ID - end

//************************** GET LAST ID
function get_last_id($table, $publish = 1, $cc = 'a')
{
  if ($cc == 'a')
    $ccfilter = '';
  else
    $ccfilter = ' and a.ccode='.$cc;
  $q = 'select id from '.$table.' a where a.publish='.$publish.$ccfilter.' order by id desc';
  $q = mysql_query($q);
  $q = mysql_fetch_object($q);
  return $q->id;
}
//************************** GET LAST ID - end

//************************** INSERT ENTRY
function insert_entry($eid, $tableid, $publish, $edate = '')
{
  if (!$edate)
    $edate = mydate();
  $query = 'insert into entries (eid, tableid, publish, entrydate) values ('.
    $eid.', '.$tableid.', '.$publish.', "'.$edate.'");';
  $query = mysql_query($query);
}
//************************** INSERT ENTRY - end

//************************** UPDATE ENTRY
function update_entry($eid, $tableid, $publish)
{
  $query = 'update entries set publish='.$publish.
    ' where entries.eid='.$eid.' and entries.tableid='.$tableid;
  $query = mysql_query($query);
}
//************************** UPDATE ENTRY - end

//************************** DELETE ENTRY
function delete_entry($eid, $tableid)
{
  $query = 'delete from entries'.
    ' where entries.eid='.$eid.' and entries.tableid='.$tableid;
  $query = mysql_query($query);
}
//************************** DELETE ENTRY - end

//************************** DELETE CONFIRM
function delete_confirm($table, $d_id, $cc, $spage)
{
  global $PHP_SELF;
  $query= 'select title from '.$table.' a where '.
    'a.id='.$d_id;
  $d_r =  mysql_query($query);
  $num = mysql_num_rows($d_r);
  if ($num == 0)
    echo 'رکوردی با این شماره پیدا نشد.';
  else
  {
    $d_r1 = mysql_fetch_object($d_r);
    echo $d_r1->title.'<br>';
    ?>
    <br>
    <form method="post"
      action="<? echo $PHP_SELF; ?>?cc=<? echo $cc;
       ?>&id=<?php echo $d_id;
        ?>&tablename=<? echo $table; ?>&spage=<? echo $spage; ?>">
      <input type="submit" name="vardel_submit" value="  حذف  ">
    </form>
    <?php
  }
}
//************************** DELETE CONFIRM - end

//************************** DELETE CONFIRM ARRAY
function delete_confirm_ar($table, &$d_ids, $cc, $spage)
{
  global $PHP_SELF;
  foreach($d_ids as $index=>$on)
  {
    $q = 'select title from '.$table.' a where a.id='.$index;
    $q = mysql_query($q);
    $q = mysql_fetch_object($q);
    echo $q->title.'<br>';
  }
  ?>
  <br>
  <form method="post" action="<? echo $PHP_SELF; ?>?cc=<? echo $cc;
   ?>&tablename=<? echo $table; ?>&spage=<? echo $spage; ?>">
  <?
  $_SESSION['ids'] = $d_ids;
  ?>
    <input type="submit" name="vardel_submit" value="  حذف  ">
  </form>
  <?
}
//************************** DELETE CONFIRM ARRAY - end

//************************** DELETE VARIABLE
function delete_var($table, $id)
{
  $query= 'select title from '.$table.' a where '.
    'a.id='.$id;
  $d_var =  mysql_query($query);
  $num = mysql_num_rows($d_var);
  if ($num == 0)
  {
    $m = 0;
  }
  else
  {
    $d_var1 = mysql_fetch_object($d_var);
    $query = 'delete from '.$table.' where '.$table.'.id='.$id;
    $d_var = mysql_query($query);
    $num = mysql_affected_rows();
    delete_entry($id, get_table_id($table));
    $m = $num;
  }
  return $m;
}
//************************** DELETE VARIABLE - end

//************************** MYDATE
function mydate()
{
  return date('Y-m-d');
}
//************************** MYDATE - end

//************************** SERVER URL
function server_url()
{   
   $proto = "http" .
       ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == "on") ? "s" : "") . "://";
   $server = isset($_SERVER['HTTP_HOST']) ?
       $_SERVER['HTTP_HOST'] : $_SERVER['SERVER_NAME'];
   return $proto . $server;
}
//************************** SERVER URL - end

//************************** REDIRECT RELATIVE
function redirect_rel($relative_url, $mymsg = '', $haveparam = 0)
{
   $url = server_url() . dirname($_SERVER['PHP_SELF']) . "/" . $relative_url;
   if ($haveparam == 1)
     $m = '&';
   else
     if ($haveparam == 0)
       $m = '?';
   $url .= $m.'mymsg='.$mymsg;
   if (!headers_sent())
   {
       header('Location: $url');
   }
   else
   {
       echo "<meta http-equiv=\"refresh\" content=\"0;url=$url\">\r\n";
   }
}
//************************** REDIRECT RELATIVE - end

//************************** INSERT NEW MARTYR
function insert_new_martyr()
{
  $cc = $_REQUEST['cc'];

  $table = 'martyrs';
  $i_martyr_name = $_POST['martyr_name'];
  $i_martyr_family = $_POST['martyr_family'];
  $i_martyr_faname = $_POST['martyr_faname'];
  $i_martyr_pbirth = $_POST['martyr_pbirth'];
  $i_martyr_bdate = $_POST['martyr_bdate'];
  if ($cc == 2)
  {
    $i_martyr_pmartyrdom = $_POST['martyr_pmartyrdom'];
    $i_martyr_mdate = $_POST['martyr_mdate'];
    $i_martyr_testament = $_POST['martyr_testament'];
    $cct = 'شهيد';
  }
  else
  {
    $i_martyr_pmartyrdom = 'null';
    $i_martyr_mdate = 'null';
    $i_martyr_testament = 'null';
    $cct = 'رزمنده';
  }
  $i_martyr_bio = $_POST['martyr_bio'];
  $i_martyr_publish = $_POST['martyr_publish'];
  if ($i_martyr_publish == 'on')
  {
    $i_martyr_publish = 1;
  }
  else
  {
    $i_martyr_publish = 0;
  }
  $mt = $cct.' '.$i_martyr_name.' '.$i_martyr_family;
  $query='insert into '.$table.' (name, family, title, faname, pbirth, bdate,'.
    ' pmartyrdom, mdate, bio, testament, ccode, publish) values ('.
    '"'.$i_martyr_name.'", "'.$i_martyr_family.'", "'.$mt.'", "'.
    $i_martyr_faname.'", "'.$i_martyr_pbirth.'", '.  '"'.$i_martyr_bdate.
    '", "'.$i_martyr_pmartyrdom.'", "'.$i_martyr_mdate.'", "'.
    $i_martyr_bio.'", "'.$i_martyr_testament.'", '.$cc.','.$i_martyr_publish.');';
  $insertnewmartyr = mysql_query($query);
  $martyrinsertno = mysql_affected_rows();
  $mymsg = '<b>'.$martyrinsertno.'</b> رکورد ثبت شد.';
  $lid = get_last_id($table);
  insert_entry($lid, get_table_id($table), $i_martyr_publish);
  if (file_exists(TMP_PIC_DIR.TMP_PIC))
  {
    $elid = get_last_id('entries');
    insert_into_album($elid , 1);
    copy(TMP_PIC_DIR.TMP_PIC, MARTYRS_PIC_DIR.$elid.'-'.get_last_id('album').'.jpg');
    unlink(TMP_PIC_DIR.TMP_PIC);
  }

  redirect_rel('martyrs.php?cc='.$cc, $mymsg, 1);
}
//************************** INSERT NEW MARTYR - end

//************************** UPDATE MARTYR
function update_martyr($cc, $e_martyr_id)
{
  $table = 'martyrs';
  $query = 'select name from '.$table.' a where '.
    'a.id='.$e_martyr_id;
  $e_martyr = mysql_query($query);
  $num = mysql_num_rows($e_martyr);
  if ($num == 0)
    $mymsg = 'رکوردی با این شماره پیدا نشد.';
  else
  {
    $e_martyr_name = $_POST['martyr_name'];
    $e_martyr_family = $_POST['martyr_family'];
    $e_martyr_faname = $_POST['martyr_faname'];
    $e_martyr_pbirth = $_POST['martyr_pbirth'];
    $e_martyr_bdate = $_POST['martyr_bdate'];
    $e_martyr_pic_delete = $_POST['pic_delete'];
    if ($cc == 2)
    {
      $e_martyr_pmartyrdom = $_POST['martyr_pmartyrdom'];
      $e_martyr_mdate = $_POST['martyr_mdate'];
      $e_martyr_testament = $_POST['martyr_testament'];
      $cct = 'شهيد';
    }
    else
    {
      $e_martyr_pmartyrdom = 'null';
      $e_martyr_mdate = 'null';
      $e_martyr_testament = 'null';
      $cct = 'رزمنده';
    }
    $e_martyr_bio = $_POST['martyr_bio'];
    $e_martyr_publish = $_POST['martyr_publish'];
    if ($e_martyr_publish == 'on')
    {
      $e_martyr_publish = 1;
    }
    else
    {
      $e_martyr_publish = 0;
    }
    $mt = $cct.' '.$e_martyr_name.' '.$e_martyr_family;
    $query = 'update '.$table.' set '.
      'name="'.$e_martyr_name.'", family="'.$e_martyr_family.
      '", title="'.$mt.
      '", faname="'.$e_martyr_faname.'", pbirth="'.$e_martyr_pbirth.'", '.
      'bdate="'.$e_martyr_bdate.'", pmartyrdom="'.$e_martyr_pmartyrdom.
      '", mdate="'.$e_martyr_mdate.'", bio="'.$e_martyr_bio.
      '", testament="'.$e_martyr_testament.'", publish='.$e_martyr_publish.
      ' where '.$table.'.id='.$e_martyr_id;
    $martyredit = mysql_query($query);
    $martyreditedno = mysql_affected_rows();
    $mymsg = '<br><br><b>تغییرات '.$martyreditedno.'</b> رکورد ثبت شد.';
    update_entry($e_martyr_id, get_table_id($table), $e_martyr_publish);
    $eid = get_entry_id(get_table_id($table), $e_martyr_id);
    if (file_exists(TMP_PIC_DIR.TMP_PIC))
    {
      $aid = get_album_id($eid);
      if (!$aid)
      {
        insert_into_album($eid , 1);
        copy(TMP_PIC_DIR.TMP_PIC, MARTYRS_PIC_DIR.$eid.'-'.get_last_id('album').'.jpg');
      }
      else
      {
        copy(TMP_PIC_DIR.TMP_PIC, MARTYRS_PIC_DIR.
          $eid.'-'.get_album_id($eid).'.jpg');
      }
      unlink(TMP_PIC_DIR.TMP_PIC);
    }
    else
    {
      if ($e_martyr_pic_delete == 'on')
      {
        @unlink(MARTYRS_PIC_DIR.
          $eid.'-'.get_album_id($eid).'.jpg');
        delete_from_album($eid);
      }
    }
  }

  redirect_rel('martyrs.php?cc='.$cc, $mymsg, 1);
}
//************************** UPDATE MARTYR - end

//************************** INSERT NEW ASAR
function insert_new_asar()
{
  $table = 'asar';
  $i_asar_pid = $_POST['asar_pid'];
  $i_asar_title = $_POST['asar_title'];
  $i_asar_text = $_POST['asar_text'];
  $i_asar_publish = $_POST['asar_publish'];
  if ($i_asar_publish == 'on')
  {
    $i_asar_publish = 1;
  }
  else
  {
    $i_asar_publish = 0;
  }
  $query='insert into '.$table.' (pid, title, text, publish) values ('.
    $i_asar_pid.', "'.$i_asar_title.
    '", "'.$i_asar_text.'", '.$i_asar_publish.');';
  $insertnewasar = mysql_query($query);
  $asarinsertno = mysql_affected_rows();
  $mymsg = '<br><br><b>'.$asarinsertno.'</b> رکورد ثبت شد.';
  insert_entry(get_last_id($table), get_table_id($table), $i_asar_publish);
  if (file_exists(TMP_PIC_DIR.TMP_PIC))
  {
    $elid = get_last_id('entries');
    insert_into_album($elid , 1);
    copy(TMP_PIC_DIR.TMP_PIC, ASAR_PIC_DIR.$elid.'-'.get_last_id('album').'.jpg');
    unlink(TMP_PIC_DIR.TMP_PIC);
  }

  redirect_rel('asar.php', $mymsg, 0);
}
//************************** INSERT NEW ASAR - end

//************************** UPDATE ASAR
function update_asar($e_asar_id)
{
  $table = 'asar';
  $query = 'select title from '.$table.' a where '.
    'a.id='.$e_asar_id;
  $e_asar = mysql_query($query);
  $num = mysql_num_rows($e_asar);
  if ($num == 0)
    $mymsg = 'رکوردی با این شماره پیدا نشد.';
  else
  {
    $e_asar_title = $_POST['asar_title'];
    $e_asar_pid = $_POST['asar_pid'];
    $e_asar_text = $_POST['asar_text'];
    $e_asar_publish = $_POST['asar_publish'];
    $e_asar_pic_delete = $_POST['pic_delete'];
    if ($e_asar_publish == 'on')
    {
      $e_asar_publish = 1;
    }
    else
    {
      $e_asar_publish = 0;
    }
    $query = 'update '.$table.' set '.
      'title="'.$e_asar_title.'", text="'.$e_asar_text.
      '", pid='.$e_asar_pid.', publish='.$e_asar_publish.
      ' where '.$table.'.id='.$e_asar_id;
    $asaredit = mysql_query($query);
    $no = mysql_affected_rows();
    $mymsg = '<br><br><b>تغییرات '.$no.'</b> رکورد ثبت شد.';
    update_entry($e_asar_id, get_table_id($table), $e_asar_publish);
    $eid = get_entry_id(get_table_id($table), $e_asar_id);
    if (file_exists(TMP_PIC_DIR.TMP_PIC))
    {
      $aid = get_album_id($eid);
        if (!$aid)
      {
        insert_into_album($eid , 1);
        copy(TMP_PIC_DIR.TMP_PIC, ASAR_PIC_DIR.$eid.'-'.get_last_id('album').'.jpg');
      }
      else
      {
        copy(TMP_PIC_DIR.TMP_PIC, ASAR_PIC_DIR.
          $eid.'-'.get_album_id($eid).'.jpg');
      }
      unlink(TMP_PIC_DIR.TMP_PIC);
    }
    else
    {
      if ($e_asar_pic_delete == 'on')
      {
        @unlink(ASAR_PIC_DIR.
          $eid.'-'.get_album_id($eid).'.jpg');
        delete_from_album($eid);
      }
    }
  }
  
  redirect_rel('asar.php', $mymsg, 0);
}
//************************** UPDATE ASAR - end

//************************** INSERT NEW NEWS
function insert_new_news()
{
  $table= 'news';
  $i_news_ndate = $_POST['news_ndate'];
  $i_news_title = $_POST['news_title'];
  $i_news_text = $_POST['news_text'];
  $i_news_summary = $_POST['news_summary'];
  $i_news_publish = $_POST['news_publish'];
  if ($i_news_publish == 'on')
  {
    $i_news_publish = 1;
  }
  else
  {
    $i_news_publish = 0;
  }
  $query='insert into '.$table.' (ndate, title, text, summary, publish) values ('.
    '"'.$i_news_ndate.'", "'.$i_news_title.
    '", "'.$i_news_text.'", "'.$i_news_summary.'", '.$i_news_publish.');';
  $insertnewnews = mysql_query($query);
  $newsinsertno = mysql_affected_rows();
  $mymsg = '<br><br><b>'.$newsinsertno.'</b> رکورد ثبت شد.';
  insert_entry(get_last_id($table), get_table_id($table), $i_news_publish);
  if (file_exists(TMP_PIC_DIR.TMP_PIC))
  {
    $elid = get_last_id('entries');
    insert_into_album($elid , 1);
    copy(TMP_PIC_DIR.TMP_PIC, NEWS_PIC_DIR.$elid.'-'.get_last_id('album').'.jpg');
    unlink(TMP_PIC_DIR.TMP_PIC);
  }

  redirect_rel('news.php', $mymsg, 0);
}
//************************** INSERT NEW NEWS - end

//************************** UPDATE NEWS
function update_news($e_news_id)
{
  $table = 'news';
  $query = 'select title from '.$table.' a where '.
    'a.id='.$e_news_id;
  $e_news = mysql_query($query);
  $num = mysql_num_rows($e_news);
  if ($num == 0)
    $mymsg = 'رکوردی با این شماره پیدا نشد.';
  else
  {
    $e_news_title = $_POST['news_title'];
    $e_news_ndate = $_POST['news_ndate'];
    $e_news_text = $_POST['news_text'];
    $e_news_summary = $_POST['news_summary'];
    $e_news_publish = $_POST['news_publish'];
    $e_news_pic_delete = $_POST['pic_delete'];
    if ($e_news_publish == 'on')
    {
      $e_news_publish = 1;
    }
    else
    {
      $e_news_publish = 0;
    }
    $query = 'update news set '.
      'title="'.$e_news_title.'", text="'.$e_news_text.
      '", ndate="'.$e_news_ndate.'", summary="'.$e_news_summary.'", '.
      'publish='.$e_news_publish.
      ' where news.id='.$e_news_id;
    $newsedit = mysql_query($query);
    $no = mysql_affected_rows();
    $mymsg = '<br><br><b>تغییرات '.$no.'</b> رکورد ثبت شد.';
    update_entry($e_news_id, get_table_id($table), $e_news_publish);
    $eid = get_entry_id(get_table_id($table), $e_news_id);
    if (file_exists(TMP_PIC_DIR.TMP_PIC))
    {
      $aid = get_album_id($eid);
        if (!$aid)
      {
        insert_into_album($eid , 1);
        copy(TMP_PIC_DIR.TMP_PIC, NEWS_PIC_DIR.$eid.'-'.get_last_id('album').'.jpg');
      }
      else
      {
        copy(TMP_PIC_DIR.TMP_PIC, NEWS_PIC_DIR.
          $eid.'-'.get_album_id($eid).'.jpg');
      }
      unlink(TMP_PIC_DIR.TMP_PIC);
    }
    else
    {
      if ($e_news_pic_delete == 'on')
      {
        @unlink(NEWS_PIC_DIR.
          $eid.'-'.get_album_id($eid).'.jpg');
        delete_from_album($eid);
      }
    }
  }

  redirect_rel('news.php', $mymsg, 0);
}
//************************** UPDATE NEWS - end

//************************** INSERT NEW MCATEGORY
function insert_new_mcategory()
{
  $table = 'mcategories';
  $i_mcategory_title = $_POST['mcategory_title'];
  $i_mcategory_description = $_POST['mcategory_description'];
  $query='insert into '.$table.' (title, description) values ('.
    '"'.$i_mcategory_title.'", "'.$i_mcategory_description.'");';
  $insertnewmcategory = mysql_query($query);
  $mcategoryinsertno = mysql_affected_rows();
  $mymsg = '<br><br><b>'.$mcategoryinsertno.'</b> رکورد ثبت شد.';

  redirect_rel('mcategories.php', $mymsg, 0);
}
//************************** INSERT NEW MCATEGORY - end

//************************** UPDATE MCATEGORY
function update_mcategory($e_mcategory_id)
{
  $table = 'mcategories';
  $query = 'select title from '.$table.' a where '.
    'a.id='.$e_mcategory_id;
  $e_mcategory = mysql_query($query);
  $num = mysql_num_rows($e_mcategory);
  if ($num == 0)
    echo 'رکوردی با این شماره پیدا نشد.';
  else
  {
    $e_mcategory_title = $_POST['mcategory_title'];
    $e_mcategory_description = $_POST['mcategory_description'];
    $query = 'update '.$table.' set '.
      'title="'.$e_mcategory_title.'", description="'.$e_mcategory_description.
      '" where '.$table.'.id='.$e_mcategory_id;
    $mcategoryedit = mysql_query($query);
    $mcategoryeditedno = mysql_affected_rows();
    $mymsg = '<br><br><b>تغییرات '.$mcategoryeditedno.'</b> رکورد ثبت شد.';
  }

  redirect_rel('mcategories.php', $mymsg, 0);
}
//************************** UPDATE MCATEGORY - end

//************************** INSERT NEW LCATEGORY
function insert_new_lcategory()
{
  $table = 'lcategories';
  $i_lcategory_title = $_POST['lcategory_title'];
  $i_lcategory_description = $_POST['lcategory_description'];
  $query='insert into lcategories (title, description) values ('.
    '"'.$i_lcategory_title.'", "'.$i_lcategory_description.'");';
  $insertnewlcategory = mysql_query($query);
  $lcategoryinsertno = mysql_affected_rows();
  $mymsg = '<br><br><b>'.$lcategoryinsertno.'</b> رکورد ثبت شد.';

  redirect_rel('lcategories.php', $mymsg, 0);
}
//************************** INSERT NEW LCATEGORY - end

//************************** UPDATE LCATEGORY
function update_lcategory($e_lcategory_id)
{
  $table = 'lcategories';
  $query = 'select title from '.$table.' a where '.
    'a.id='.$e_lcategory_id;
  $e_lcategory = mysql_query($query);
  $num = mysql_num_rows($e_lcategory);
  if ($num == 0)
    echo 'رکوردی با این شماره پیدا نشد.';
  else
  {
    $e_lcategory_title = $_POST['lcategory_title'];
    $e_lcategory_description = $_POST['lcategory_description'];
    $query = 'update lcategories set '.
      'title="'.$e_lcategory_title.'", description="'.$e_lcategory_description.
      '" where lcategories.id='.$e_lcategory_id;
    $lcategoryedit = mysql_query($query);
    $lcategoryeditedno = mysql_affected_rows();
    $mymsg = '<br><br><b>تغییرات '.$lcategoryeditedno.'</b> رکورد ثبت شد.';
  }

  redirect_rel('lcategories.php', $mymsg, 0);
}
//************************** UPDATE LCATEGORY - end

//************************** INSERT NEW MTEXT
function insert_new_mtext()
{
  $table = 'mtexts';
  $i_mtext_ccode = $_POST['mtext_ccode'];
  $i_mtext_title = $_POST['mtext_title'];
  $i_mtext_text = $_POST['mtext_text'];
  $i_mtext_author = $_POST['mtext_author'];
  $i_mtext_publish = $_POST['mtext_publish'];
  if ($i_mtext_publish == 'on')
  {
    $i_mtext_publish = 1;
  }
  else
  {
    $i_mtext_publish = 0;
  }
  $query='insert into '.$table.' (ccode, title, text, author, publish) values ('.
    '"'.$i_mtext_ccode.'", "'.$i_mtext_title.
    '", "'.$i_mtext_text.'", "'.$i_mtext_author.'", '.$i_mtext_publish.');';
  $insertnewmtext = mysql_query($query);
  $mtextinsertno = mysql_affected_rows();
  $mymsg = '<br><br><b>'.$mtextinsertno.'</b> رکورد ثبت شد.';
  insert_entry(get_last_id($table), get_table_id($table), $i_mtext_publish);

  redirect_rel('mtexts.php', $mymsg, 0);
}
//************************** INSERT NEW MTEXT - end

//************************** UPDATE MTEXT
function update_mtext($e_mtext_id)
{
  $table = 'mtexts';
  $query = 'select title from '.$table.' a where '.
    'a.id='.$e_mtext_id;
  $e_mtext = mysql_query($query);
  $num = mysql_num_rows($e_mtext);
  if ($num == 0)
    $mymsg = 'رکوردی با این شماره پیدا نشد.';
  else
  {
    $e_mtext_title = $_POST['mtext_title'];
    $e_mtext_ccode = $_POST['mtext_ccode'];
    $e_mtext_text = $_POST['mtext_text'];
    $e_mtext_author = $_POST['mtext_author'];
    $e_mtext_publish = $_POST['mtext_publish'];
    if ($e_mtext_publish == 'on')
    {
      $e_mtext_publish = 1;
    }
    else
    {
      $e_mtext_publish = 0;
    }
    $query = 'update mtexts set '.
      'title="'.$e_mtext_title.'", text="'.$e_mtext_text.
      '", ccode='.$e_mtext_ccode.', author="'.$e_mtext_author.'", '.
      'publish='.$e_mtext_publish.
      ' where mtexts.id='.$e_mtext_id;
    $mtextedit = mysql_query($query);
    $no = mysql_affected_rows();
    $mymsg = '<br><br><b>تغییرات '.$no.'</b> رکورد ثبت شد.';
    update_entry($e_mtext_id, get_table_id($table), $e_mtext_publish);
  }

  redirect_rel('mtexts.php', $mymsg, 0);
}
//************************** UPDATE MTEXT - end

//************************** INSERT NEW LTEXT
function insert_new_ltext()
{
  $table = 'ltexts';
  $i_ltext_ccode = $_POST['ltext_ccode'];
  $i_ltext_title = $_POST['ltext_title'];
  $i_ltext_text = $_POST['ltext_text'];
  $i_ltext_author = $_POST['ltext_author'];
  $i_ltext_publish = $_POST['ltext_publish'];
  if ($i_ltext_publish == 'on')
  {
    $i_ltext_publish = 1;
  }
  else
  {
    $i_ltext_publish = 0;
  }
  $query='insert into '.$table.' (ccode, title, text, author, publish) values ('.
    '"'.$i_ltext_ccode.'", "'.$i_ltext_title.
    '", "'.$i_ltext_text.'", "'.$i_ltext_author.'", '.$i_ltext_publish.');';
  $insertnewltext = mysql_query($query);
  $ltextinsertno = mysql_affected_rows();
  $mymsg = '<br><br><b>'.$ltextinsertno.'</b> رکورد ثبت شد.';
  insert_entry(get_last_id($table), get_table_id($table), $i_ltext_publish);

  redirect_rel('ltexts.php', $mymsg, 0);
}
//************************** INSERT NEW LTEXT - end

//************************** UPDATE LTEXT
function update_ltext($e_ltext_id)
{
  $table = 'ltexts';
  $query = 'select title from ltexts a where '.
    'a.id='.$e_ltext_id;
  $e_ltext = mysql_query($query);
  $num = mysql_num_rows($e_ltext);
  if ($num == 0)
    $mymsg = 'رکوردی با این شماره پیدا نشد.';
  else
  {
    $e_ltext_title = $_POST['ltext_title'];
    $e_ltext_ccode = $_POST['ltext_ccode'];
    $e_ltext_text = $_POST['ltext_text'];
    $e_ltext_author = $_POST['ltext_author'];
    $e_ltext_publish = $_POST['ltext_publish'];
    if ($e_ltext_publish == 'on')
    {
      $e_ltext_publish = 1;
    }
    else
    {
      $e_ltext_publish = 0;
    }
    $query = 'update ltexts set '.
      'title="'.$e_ltext_title.'", text="'.$e_ltext_text.
      '", ccode='.$e_ltext_ccode.', author="'.$e_ltext_author.'", '.
      'publish='.$e_ltext_publish.
      ' where ltexts.id='.$e_ltext_id;
   $ltextedit = mysql_query($query);
   $no = mysql_affected_rows();
   $mymsg = '<br><br><b>تغییرات '.$no.'</b> رکورد ثبت شد.';
   update_entry($e_ltext_id, get_table_id($table), $e_ltext_publish);
  }

  redirect_rel('ltexts.php', $mymsg, 0);
}
//************************** UPDATE LTEXT - end

//************************** VARDELM
function vardelm($pic_dir = '')
{
  $cc = $_REQUEST['cc'];
  if (($cc <> 1) and ($cc <> 2))
    $cc = $_POST['cc'];

  $table = $_POST['tablename'];
  if (!$table)
    $table = $_REQUEST['tablename'];
  $spage = $_POST['spage'];
  if (!$spage)
    $spage = $_REQUEST['spage'];
  $d_id = $_REQUEST['id'];
  $d_ids = $_POST['ids'];
  $m = 0;

  $tid = get_table_id($table);
  if ($d_id)
  {
    if ($pic_dir != '')
    {
      $eid = get_entry_id($tid, $d_id);
      @unlink($pic_dir.
        $eid.'-'.get_album_id($eid).'.jpg');
      delete_from_album($eid);
    }
    $m += delete_var($table, $d_id);
  }
  else
    if ($d_ids)
    {
      foreach($d_ids as $index=>$on)
      {
        if ($pic_dir != '')
        {
          $eid = get_entry_id($tid, $index);
          @unlink($pic_dir.
            $eid.'-'.get_album_id($eid).'.jpg');
          delete_from_album($eid);
        }
        $m += delete_var($table, $index);
      }
    }

  if (!$m)
    $mymsg = 'ركوردي با اين شماره پيدا نشد و يا قابل حذف نبود.';
  else
    $mymsg = $m.' ركورد حذف شد.';

  echo $mymsg.'<br>';
}
//************************** VARDELM - end

//************************** GET RECORD
function get_record($table, $id)
{
  $q = 'select * from '.$table.' a where a.id='.$id;
  $q = mysql_query($q);
  if (mysql_num_rows($q))
  {
    $q = mysql_fetch_object($q);
    return $q;
  }
  else
    return 0;
}
//************************** GET RECORD - end

//************************** GET ENTRY ID
function get_entry_id($tableid, $eid)
{
  $q = 'select id from entries a where a.tableid='.$tableid.
    ' and a.eid='.$eid;
  $q = mysql_query($q);
  $q = mysql_fetch_object($q);
  return $q->id;
}
//************************** GET ENTRY ID - end

//************************** GET ENTRY TID
function get_entry_tid($id)
{
  $q = 'select tableid, eid from entries a where a.id='.$id;
  $q = mysql_query($q);
  $q = mysql_fetch_object($q);
  return $q;
}
//************************** GET ENTRY TID - end

//************************** GET PERSONAL TITLE
function get_personal_title($id)
{
  $q = 'select title from martyrs a where a.id='.$id;
  $q = mysql_query($q);
  $q = mysql_fetch_object($q);
  return $q->title;
}
//************************** GET PERSONAL TITLE - end

//************************** UPLOAD FILE
function upload_file($target_dir, $tmp = 0)
{
  $userfile=$_FILES['userfile']['tmp_name'];
  $userfile_name=$_FILES['userfile']['name'];
  $userfile_size=$_FILES['userfile']['size'];
  $userfile_type=$_FILES['userfile']['type'];
  $userfile_error=$_FILES['userfile']['error'];
  $filename = basename($userfile_name);
  if (!$tmp)
    $t = TMP_PIC;
  else
    $t = $filename;
  if ($userfile_size<=0)
    echo "$filename خالي است يا امكان بارگذاري آن وجود ندارد.<br>";
  if (!@copy($userfile, "$target_dir$t"))
  {
    echo "عدم توانايي در كپي فايل.<br>";
    return;
  }
 // echo "$filename با موفقيت بارگذاري شد.<br>";
//  echo "File size: ".number_format($userfile_size)."<br>";
  //echo "File type: $userfile_type<br>";
  return $target_dir.$filename;
}
//************************** UPLOAD FILE - end

//************************** INSERT INTO ALBUM
function insert_into_album($id, $prim = 0)
{
  $ins = 'insert into album (pid, prim) values('.
    $id.', '.$prim.');';
  $ins = mysql_query($ins);
  $nins = mysql_affected_rows();
  return $nins;
}
//************************** INSERT INTO ALBUM - end

//************************** GET ALBUM ID
function get_album_id($pid)
{
  $id = 'select id from album a where a.prim=1 and a.pid='.$pid;
  $id = mysql_query($id);
  $id = mysql_fetch_object($id);
  return $id->id;
}
//************************** GET ALBUM ID - end

//************************** GET ALBUM ID
function delete_from_album($pid)
{
  $id = 'delete from album where album.prim=1 and album.pid='.$pid;
  $id = mysql_query($id);
  $id = mysql_affected_rows();
  return $id;
}
//************************** GET ALBUM ID - end

//************************** INSERT NEW REMEMBRANCE
function insert_new_remembrance()
{
  $table = 'remembrances';
  $i_remembrance_pid = $_POST['remembrance_pid'];
  $i_remembrance_title = $_POST['remembrance_title'];
  $i_remembrance_text = $_POST['remembrance_text'];
  $i_remembrance_publish = $_POST['remembrance_publish'];
  if ($i_remembrance_publish == 'on')
  {
    $i_remembrance_publish = 1;
  }
  else
  {
    $i_remembrance_publish = 0;
  }
  $query='insert into '.$table.' (pid, title, text, publish) values ('.
    $i_remembrance_pid.', "'.$i_remembrance_title.
    '", "'.$i_remembrance_text.'", '.$i_remembrance_publish.');';
  $insertnewremembrance = mysql_query($query);
  $remembranceinsertno = mysql_affected_rows();
  $mymsg = '<br><br><b>'.$remembranceinsertno.'</b> رکورد ثبت شد.';
  insert_entry(get_last_id($table), get_table_id($table), $i_remembrance_publish);
  if (file_exists(TMP_PIC_DIR.TMP_PIC))
  {
    $elid = get_last_id('entries');
    insert_into_album($elid , 1);
    copy(TMP_PIC_DIR.TMP_PIC, REMEMBRANCES_PIC_DIR.$elid.'-'.get_last_id('album').'.jpg');
    unlink(TMP_PIC_DIR.TMP_PIC);
  }

  redirect_rel('remembrances.php', $mymsg, 0);
}
//************************** INSERT NEW REMEMBRANCE - end

//************************** UPDATE REMEMBRANCE
function update_remembrance($e_remembrance_id)
{
  $table = 'remembrances';
  $query = 'select title from '.$table.' a where '.
    'a.id='.$e_remembrance_id;
  $e_remembrances = mysql_query($query);
  $num = mysql_num_rows($e_remembrances);
  if ($num == 0)
    $mymsg = 'رکوردی با این شماره پیدا نشد.';
  else
  {
    $e_remembrance_title = $_POST['remembrance_title'];
    $e_remembrance_pid = $_POST['remembrance_pid'];
    $e_remembrance_text = $_POST['remembrance_text'];
    $e_remembrance_publish = $_POST['remembrance_publish'];
    $e_remembrance_pic_delete = $_POST['pic_delete'];
    if ($e_remembrance_publish == 'on')
    {
      $e_remembrance_publish = 1;
    }
    else
    {
      $e_remembrance_publish = 0;
    }
    $query = 'update '.$table.' set '.
      'title="'.$e_remembrance_title.'", text="'.$e_remembrance_text.
      '", pid='.$e_remembrance_pid.', publish='.$e_remembrance_publish.
      ' where '.$table.'.id='.$e_remembrance_id;
    $remembranceedit = mysql_query($query);
    $no = mysql_affected_rows();
    $mymsg = '<br><br><b>تغییرات '.$no.'</b> رکورد ثبت شد.';
    update_entry($e_remembrance_id, get_table_id($table), $e_remembrance_publish);
    $eid = get_entry_id(get_table_id($table), $e_remembrance_id);
    if (file_exists(TMP_PIC_DIR.TMP_PIC))
    {
      $aid = get_album_id($eid);
        if (!$aid)
      {
        insert_into_album($eid , 1);
        copy(TMP_PIC_DIR.TMP_PIC, REMEMBRANCES_PIC_DIR.$eid.'-'.get_last_id('album').'.jpg');
      }
      else
      {
        copy(TMP_PIC_DIR.TMP_PIC, REMEMBRANCES_PIC_DIR.
          $eid.'-'.get_album_id($eid).'.jpg');
      }
      unlink(TMP_PIC_DIR.TMP_PIC);
    }
    else
    {
      if ($e_remembrance_pic_delete == 'on')
      {
        @unlink(REMEMBRANCES_PIC_DIR.
          $eid.'-'.get_album_id($eid).'.jpg');
        delete_from_album($eid);
      }
    }
  }
  
  redirect_rel('remembrances.php', $mymsg, 0);
}
//************************** UPDATE REMEMBRANCE - end

//************************** GET NUM OF RECORDS
function get_num_of_records($table = 'entries', $publish = 1)
{
  $q = 'select id from '.$table.' a where a.publish='.$publish;
  $q = mysql_query($q);
  return mysql_num_rows($q);
}
//************************** GET NUM OF RECORDS - end

//************************** GET NUM OF RECORDS MARTYRS
function get_num_of_records_martyrs($cc = 2, $publish = 1)
{
  
  $q = 'select id from martyrs a where a.publish='.$publish.
    ' and a.ccode='.$cc;
  $q = mysql_query($q);
  return mysql_num_rows($q);
}
//************************** GET NUM OF RECORDS MARTYRS - end

//************************** GET NUM OF RECORDS MARTYRDOM
function get_num_of_records_martyrdom($cc = 0, $ml = 'm', $publish = 1)
{
  if ($cc == 0)
    $cf = '';
  else
    $cf = ' and a.ccode='.$cc;
  $q = 'select id from '.$ml.'texts a where a.publish='.$publish.
    $cf;
  $q = mysql_query($q);
  return mysql_num_rows($q);
}
//************************** GET NUM OF RECORDS MARTYRDOM - end

//************************** GET USER NNAME
function get_user_nname($user_name)
{
  $q = 'select name from authors a where a.uname="'.$user_name.'"';
  $q = mysql_query($q);
  if (@mysql_num_rows($q))
  {
    $q = mysql_fetch_object($q);
    return $q->name;
  }
  else
    return false;
}
//************************** GET USER NNAME - end

//************************** LOGIN
function login($user_name, $user_pass)
{
  if (isset($_SESSION['v_user']))
    return false;
  mydbconnect();
  $q = 'select * from authors a where a.uname="'.$user_name.
    '" and a.pass=password("'.$user_pass.'");';
  $q = mysql_query($q);
  if (@mysql_num_rows($q))
    return true;
  return false;
}
//************************** LOGIN - end

//************************** IS FILLED
function is_filled($f)
{
  foreach ($f as $i=>$v)
    if (!isset($i) || ($v == ''))
      return false;
  return true;
}
//************************** IS FILLED - end

//************************** INSERT NEW AUTHOR
function insert_new_author()
{
  $table= 'authors';
  $i_author_name = $_POST['author_name'];
  $i_author_uname = $_POST['author_uname'];
  $i_author_pass = $_POST['author_pass'];
  $i_author_passc = $_POST['author_passc'];
  if ($i_author_pass != $i_author_passc)
    $mymsg = 'دو روز عبور يكسان نيستند.';
  else
  {
    if (!is_filled($_POST))
      $mymsg = 'مشكل در فيلدها: همه فيلدها بايد پر شوند.';
    else
    {
	  $q = 'select uname from authors a where uname="'.$i_author_uname.'"';
	  $q = mysql_query($q);
	  if (@mysql_num_rows($q))
	    $mymsg = '<br><br>اين نام كاربري تكراري است.';
	  else
	  {
        $query='insert into '.$table.' (name, uname, pass) values ('.
          '"'.$i_author_name.'", "'.$i_author_uname.
          '", password("'.$i_author_pass.'"));';
        $insertnewauthor = mysql_query($query);
        $authorinsertno = mysql_affected_rows();
        $mymsg = '<br><br><b>'.$authorinsertno.'</b> رکورد ثبت شد.';
	  }
    }
  }
  redirect_rel('authors.php', $mymsg, 0);
}
//************************** INSERT NEW AUTHOR - end

//************************** UPDATE AUTHOR
function update_author($e_author_id)
{
  $table = 'authors';
  $query = 'select uname from '.$table.' a where '.
    'a.id='.$e_author_id;
  $e_author = mysql_query($query);
  $num = mysql_num_rows($e_author);
  if ($num == 0)
    $mymsg = 'رکوردی با این شماره پیدا نشد.';
  else
  {
    $e_author_name = $_POST['author_name'];
    $e_author_uname = $_POST['author_uname'];
    $e_author_pass_now = $_POST['author_pass_now'];
    $e_author_pass_new = $_POST['author_pass_new'];
    $e_author_pass_new_c = $_POST['author_pass_new_c'];
    if ($e_author_pass_new != $e_author_pass_new_c)
      $mymsg = 'دو روز عبور يكسان نيستند.';
    else
    {
      if (!is_filled($_POST))
        $mymsg = 'مشكل در فيلدها: همه فيلدها بايد پر شوند.';
      else
      {
        $p = 'select pass from authors a where a.id='.$e_author_id.
          ' and pass=password("'.$e_author_pass_now.'")';
        $p = mysql_query($p);
        if (@mysql_num_rows($p))
        {
          $query = 'update authors set '.
            'name="'.$e_author_name.'", uname="'.$e_author_uname.
            '", pass=password("'.$e_author_pass_new.'") '.
            ' where authors.id='.$e_author_id;
          $authoredit = mysql_query($query);
          $no = mysql_affected_rows();
          $mymsg = '<br><br><b>تغییرات '.$no.'</b> رکورد ثبت شد.';
          $e_author = mysql_fetch_object($e_author);
          if ($_SESSION['v_user'] == $e_author->uname)
            $_SESSION['v_user'] = $e_author_uname;
        }
        else
          $mymsg = '<br><br>رمز عبور اشتباه است.';
      }
    }
  }

  redirect_rel('authors.php', $mymsg, 0);
}
//************************** UPDATE AUTHOR - end

//************************** DEL AUTHOR
function del_author($d_author_id)
{
  $m_pass = $_POST['m_pass'];
  if ($m_pass && is_this_manager($m_pass))
  {
    $d = 'delete from authors where authors.id='.$d_author_id;
    $d = mysql_query($d);
    if ($n = mysql_affected_rows())
      $mymsg = '<br><br><b>'.$n.'</b> ركورد حذف شد.';
    else
      $mymsg = '<br><br>خطا در حذف';
  }
  else
    $mymsg = 'شما اجازه اين كار را نداريد.';
  
  redirect_rel('authors.php', $mymsg, 0);
}
//************************** DEL AUTHOR - end

//************************** IS THIS MANAGER
function is_this_manager($pass = '')
{
  if ($pass != '')
    $passfilter = ' and a.pass=password("'.$pass.'") ';
  else
    $passfilter = '';
  $is_m = 'select * from authors a where a.uname="'.$_SESSION['v_user'].
    '" and a.manager=1'.$passfilter;  // is manager ?
  $is_m = mysql_query($is_m);
  if (@mysql_num_rows($is_m))
    return true;
  else
    return false;
}
//************************** IS THIS MANAGER - end

?>