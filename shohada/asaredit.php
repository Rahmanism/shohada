﻿<? @session_start(); ?>
<html dir="rtl">
<?php
require_once('inc.php');
if (!isset($_SESSION['v_user']))
  redirect_rel('index.php', '', 0);
else
{
html_top('آثار', 'ويرايش اثر');

$e_asar_id = $_REQUEST['id'];
$editasar_submit = $_POST['editasar_submit'];
if ($editasar_submit)
  update_asar($e_asar_id);
else
  edit_asar_form($e_asar_id);

html_bottom();
}
?>