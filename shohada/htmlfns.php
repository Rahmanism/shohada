<?php
//************************** HTML TOP
function html_top($t = '', $h = '', $p = '')//$t = title; $h = heading; $p = type of menu;
{
  if (!$t)
    $t = 'شهدا شاهد بر باطن و حقيقت عالمند.';
/*  if (!$h)
    $h = $t;*/
  mydbconnect();
?>
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8">
  <meta http-equiv="content-language" content="fa">
  <link href="main.css" rel="stylesheet" type="text/css">
  <link rel="shortcut icon" type="image/x-icon" href="./images/i.ico" />
  <script type="text/javascript">
    _editor_url = './htmlarea3';
    _editor_lang = 'fa';
  </script>
  <script type="text/javascript" src="./j.js"></script>
  <script type="text/javascript" src="./htmlarea3/htmlarea.js"></script>
  <script type="text/javascript" src="./htmlarea3/dialog.js"></script>
  <script tyle="text/javascript" src="./htmlarea3/lang/fa.js"></script>
  <title><?php echo $t; ?></title>
</head>
<body onLoad="HTMLArea.replaceAll();">
<a name="ptop"></a>
<table>
<tr>
<td>
<?php
banner($h);
?>
</td>
</tr>
<tr>
<td>
<?php
bodytop($h, $p);
}
//************************** HTML TOP - end 

//************************** BANNER
function banner($h = '')
{
?>
<table id="banner" cellpadding="0" cellspacing="0">
  <tr>
    <td align="right" width="5">
      <img src="<? echo $main_url; ?>images/banner_right.png">
     
    </td>
    <td style="background-image:url(<? echo $main_url; ?>images/banner_bg.png);
        vertical-align: middle; background-repeat:repeat-x">
      <!--<div style="float:right; position:absolute">
        &nbsp;
        <font color="#FFFFFF" size="3" face="Times">
        سيد شهيدان اهل قلم:</font>
      </div><br />-->
      &nbsp;
      <a href="<? echo $main_url; ?>">
      <img src="images/h.gif" />
      <!--
      <span style="font-size:30px; font-family:times; color:#ffffff">
        <strong>&nbsp;&nbsp;&nbsp;اين سردار خيبر قلعه قلب مرا نيز فتح كرده است</strong>
      </span>
      -->
      </a>
    </td>
    <td style="background-image:url(<? echo $main_url; ?>images/banner_bg.png);
        vertical-align: middle; background-repeat:repeat-x; text-align:left">
        <img src="images/logo.gif" align="left"/>
<!--      <span style="font-size:20px; font-family:times; color:#ffffff">
        <strong><nobr>سردار خيبر حاج ابراهيم همت&nbsp;&nbsp;&nbsp;</nobr></strong>
      </span>-->
    </td>
    <td align="left" width="5">
      <img src="<? echo $main_url; ?>images/banner_left.png">
    </td>
  </tr>
</table>
<?php
}
//************************** BANNER - end

//************************** BODY TOP
function bodytop($h = '', $p = '')
{
?>
  <table id="page_body">
    <tr>
      <td width="15%">
        <?php bodyright($p); ?>
      </td>
      <td width="70%">
        <h3 align="center"><? echo $h; ?></h3>
<?php
}
//************************** BODY TOP - end

//************************** BODY RIGHT
function bodyright($p = '')
{
  show_user_menu($p);
  echo "<hr>\n";
  if (isset($_SESSION['v_user']))
    show_managementmenu();
}
//************************** BODY RIGHT - end

//************************** SHOW CATEGORIES
function show_user_menu($p = '')
{
?>
  <table class="menu" cellspacing="2">
    <tr>
      <th>&nbsp;</th>
      <th>موضوعات</th>
    </tr>
    <tr>
      <td>&nbsp;</td>
      <td>
        <a href="<?php echo $main_url; ?>index.php">
        <nobr>صفحه اصلي</nobr></a>
      </td>
    </tr>
  <tr>
    <td>&nbsp;</td>
    <td><a href="<? echo $main_url; ?>archive.php">
      <nobr>آرشيو</nobr></a></td>
  </tr>
  <tr>
    <td>&nbsp;</td><td>
      <a href="s_martyrs.php?cc=2&arch=0">شهدا</a>
      <? if ($p == 'martyrs?cc=2') { ?>
      <hr>&nbsp;&nbsp;<a href="s_martyrs.php?cc=2&arch=1">آرشيو&nbsp;شهدا</a><hr>
      <? } ?>
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td><td>
      <a href="s_martyrs.php?cc=1&arch=0">رزمندگان</a>
      <? if ($p == 'martyrs?cc=1') { ?>
      <hr>&nbsp;&nbsp;<a href="s_martyrs.php?cc=1&arch=1">آرشيو&nbsp;رزمندگان</a><hr>
      <? } ?>
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td><td>
      <a href="s_martyrdom.php?arch=0">شهادت</a>
      <? if ($p == 'martyrdom') { ?>
      <hr>
      &nbsp;&nbsp;<a href="s_martyrdom.php?arch=1">آرشيو&nbsp;مطالب</a>
      <?php
      $mcat = 'select id, title from mcategories';// a where a.pid=0';
      $mcat = mysql_query($mcat);
      $nmcat = mysql_num_rows($mcat);
      echo '<table class=submenu>';
      for ($i = 0; $i < $nmcat; $i++)
      {
        $mcat1 = mysql_fetch_object($mcat); 
      ?><tr><td>
        &nbsp;&nbsp;<a href="<? echo $main_url; ?>?cc=<? echo $mcat1->id; ?>">
        <nobr><? echo untitled($mcat1->title); ?></nobr></a></td></tr>
      <? }
      echo '</table><hr>';
      }?>
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td><td>
      <a href="s_moqavemat.php?arch=0">ادبيات&nbsp;و&nbsp;هنر&nbsp;مقاومت</a>
      <? if ($p == 'moqavemat') { ?>
      <hr>
      &nbsp;&nbsp;<a href="s_moqavemat.php?arch=1">آرشيو&nbsp;مطالب</a>
      <?php
      $lcat = 'select id, title from lcategories';// a where a.pid=0';
      $lcat = mysql_query($lcat);
      $nlcat = mysql_num_rows($lcat);
      echo '<table class=submenu>';
      for ($i = 0; $i < $nlcat; $i++)
      {
        $lcat1 = mysql_fetch_object($lcat); 
      ?><tr><td>
        &nbsp;&nbsp;<a href="<? echo $main_url; ?>?cc=<? echo $lcat1->id; ?>">
        <nobr><? echo untitled($lcat1->title); ?></nobr></a></td></tr>
      <? }
      echo '</table><hr>';
      }?>
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td><td>
      <a href="s_news.php?arch=0">خبر</a>
      <? if ($p == 'news') { ?>
      <hr>&nbsp;&nbsp;<a href="s_news.php?arch=1">آرشيو خبر</a><hr>
      <? } ?>
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td><td>
      <a href="s_asar.php?arch=0">آثار</a>
      <? if ($p == 'asar') { ?>
      <hr>&nbsp;&nbsp;<a href="s_asar.php?arch=1">آرشيو آثار</a><hr>
      <? } ?>
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td><td>
      <a href="s_remembrances.php?arch=0">خاطرات</a>
      <? if ($p == 'remembrances') { ?>
      <hr>&nbsp;&nbsp;<a href="s_remembrances.php?arch=1">آرشيو خاطرات</a><hr>
      <? } ?>
    </td>
  </tr>
  </table>
<?php
}
//************************** SHOW CATEGORIES - end

//************************** UNTITLED
function untitled($s)
{
  if ($s)
    return $s;
  else
    return 'بدون عنوان';
}
//************************** UNTITLED - end

//************************** SHOW MANAGEMENT MENU
function show_managementmenu()
{
?>
<table class="menu" cellspacing="2">
  <tr>
    <th>&nbsp;</th>
    <th>منوي مديريت</th>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td><a href="<? echo $main_url; ?>index.php">
      <nobr>صفحه اصلي</nobr></a></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>
      <a href="martyrs.php?cc=2">شهدا</a>
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>
      <a href="martyrs.php?cc=1">رزمندگان</a>
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>
      <a href="martyrdom.php">شهادت</a>
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;&nbsp;
      <a href="<? echo $main_url; ?>mcategories.php">
      <nobr>دسته بندی</nobr></a>
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;&nbsp;
      <a href="<? echo $main_url; ?>mtexts.php">مطالب</a>
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>
      <a href="moqavemat.php">
      <nobr>ادبيات و هنر مقاومت</nobr></a>
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;&nbsp;
      <a href="<? echo $main_url; ?>lcategories.php">
      <nobr>دسته بندی</nobr></a>
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;&nbsp;
      <a href="<? echo $main_url; ?>ltexts.php">مطالب</a>
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>
      <a href="news.php">خــبر</a>
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>
      <a href="asar.php">آثـار</a>
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>
      <a href="remembrances.php">خاطرات</a>
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>
      <a href="authors.php">نويسندگان</a>
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>
      <a href="" onClick="upload_file_win=window.open('upload.php?tdir=<? 
            echo FILES_DIR; ?>&tmp=1', 'upload_file_win', 'toolbar=no');">
      بارگذاري&nbsp;فايل</a>
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>
      <a href="index.php?lo=logout">خروج</a>
    </td>
  </tr>
</table>
<?
}
//************************** SHOW MANAGEMENT MENU - end

//************************** BODY LEFT
function bodyleft($user_msg = '')
{
?>
  <table>
    <tr>
      <td>
        <img src="<? echo $main_url; ?>images/random.jpg">
      </td>
    </tr>
  </table>
  <hr>
  <table class="menu" style="font-size: 9px;" cellspacing="2">
    <tr>
      <th>&nbsp;</th><th>جستجو</th>
    </tr>
    <tr>
      <td colspan="2">
        <form name="search_form" method="post" action="search.php">
          <input type="text" name="search_text" size="9">
          <input type="submit" value="جستجو">
        </form>
      </td>
    </tr>
  </table>
  <hr>
  <!--  Last notes -->
  <? show_last_entries();
  echo '<hr>';
  show_user_login($user_msg);
}
//************************** BODY LEFT - end

//************************** SHOW LAST ENTRIES
function show_last_entries()
{
  ?>
  <table class="menu" style="font-size: 9px;" cellspacing="2">
    <tr>
      <th>&nbsp;</th><th>آخرين مطالب</th>
    </tr>
    <?php
    $lastnotes = 'select id, eid, tableid from entries a'.
      ' where a.publish=1 order by entrydate desc, id desc limit 10';
    $lastnotes = mysql_query($lastnotes);
    $nlastnotes = mysql_num_rows($lastnotes);
    for ($nl = 0; $nl < $nlastnotes; $nl++)
    {
      $alast = mysql_fetch_object($lastnotes);
      $stable = get_table_name($alast->tableid);
      $e1 = 'select title from '.$stable.' a where a.id='.$alast->eid;
      $e1 = mysql_query($e1);
      $e1 = mysql_fetch_object($e1);
      echo "<tr><td colspan=\"2\">\n";
        echo '<b>&bull;</b> <a href="'.$main_url.'index.php?id='.$alast->id.'">';
      echo untitled($e1->title).'</a>';
      echo "</td></tr>\n";
    }
    ?>
  </table>
  <?
}
//************************** SHOW LAST ENTRIES - end

//************************** SHOW USER LOGIN
function show_user_login($user_msg = '')
{
  ?>
  <table class="menu" cellspacing="2">
    <tr>
      <th>&nbsp;</th><th>ورود</th>
    </tr>
    <tr>
      <td colspan="2">
        <?
        if (!isset($_SESSION['v_user']))
        {
        ?>
          <form name="login_form" method="post" action="index.php?lo=login">
            <label>نام كاربر</label>
            <input type="text" name="user_name"><br>
            <label>رمز عبور</label>
            <input type="password" name="user_pass">
            <div style="text-align:center"><input type="submit" value=" ورود "></div>
          </form>
        <?
        }
        else
          if ($user_msg == '')
            $user_msg = get_user_nname($_SESSION['v_user']).'، خوش آمديد.';
        echo '<div style="text-align:center">'.$user_msg.'</div>';
        ?>
      </td>
    </tr>
  </table>
  <?
}
//************************** SHOW USER LOGIN - end

//************************** BODY BOTTOM
function bodybottom($user_msg = '')
{
?>
    </td> <!-- 70% -->
    <td width="15%">
      <?php bodyleft($user_msg); ?>
    </td>
  </tr>
</table>
<?php
}
//************************** BODY BOTTOM - end

//************************** HTML BOTTOM
function html_bottom($user_msg = '')
{
  bodybottom($user_msg);
?>
    </td>
  </tr>
</table>
<a href="#ptop">^بالا^</a>
</body>
</html>
<?php
}
//************************** HTML BOTTOM - end

//************************** HTMLAREA1
function htmlarea1($id = 'TA')
{
?>
<script type="text/javascript" defer="1">
    HTMLArea.replace("<?php echo $id; ?>");
</script>
<?php
}
//************************** HTMLAREA1 - end

//************************** NEW MARTYR FORM
function new_martyr_form($cc)
{
  global $PHP_SELF;
  if (file_exists(TMP_PIC_DIR.TMP_PIC))
    unlink(TMP_PIC_DIR.TMP_PIC);
?>
  <form method="post" action="<? echo $PHP_SELF; ?>?cc=<? echo $cc; ?>">
    <table class="tform" align="center">
      <tr>
        <td><label>نام:</label></td>
        <td><input type="text" name="martyr_name" maxlength="20"></td>
        <td><label>نام خانوادگی:</label></td>
        <td><input type="text" name="martyr_family" maxlength="30"></td>
        <td><label>نام پدر:</label></td>
        <td><input type="text" name="martyr_faname" maxlength="20"></td>
      </tr>
      <tr>
        <td><label>محل تولد:</label></td>
        <td><input type="text" name="martyr_pbirth" maxlength="20"></td>
        <td><label>تاریخ تولد:</label></td>
        <td colspan="2">
          <span dir="ltr">
            <input type="text" name="martyr_bdate" maxlength="10" value="0000-00-00">
          </span>
        </td>
        <td>
          <input type="button" name="upload_image" value=" بارگذاري عكس " 
            onClick="upload_win=window.open('upload.php?tdir=<? 
            echo TMP_PIC_DIR; ?>', 'upload_win', 'toolbar=no');">
        </td>
      </tr>
      <?php
      if ($cc == 2)
      {
      ?>
      <tr>
        <td><label>محل شهادت:</label></td>
        <td><input type="text" name="martyr_pmartyrdom" maxlength="50"></td>
        <td><label>تاریخ شهادت:</label></td>
        <td colspan="2">
          <span dir="ltr">
            <input type="text" name="martyr_mdate" maxlength="10" value="0000-00-00">
          </span>
        </td>
      </tr>
      <? } ?>
      <tr>
        <td colspan="6">
          <label>زندگی‌نامه:</label><br>
          <textarea name="martyr_bio">&nbsp;</textarea>
        </td>
      </tr>
      <?php
      if ($cc == 2)
      {
      ?>
      <tr>
        <td colspan="6">
          <label>وصیت‌نامه:</label><br>
          <textarea name="martyr_testament">&nbsp;</textarea>
        </td>
      </tr>
      <? } ?>
      <tr>
        <td colspan="6">
          <label>انتشار بیابد؟</label><input type="checkbox" name="martyr_publish">
        </td>
      </tr>
      <tr>
        <td colspan="6" align="center">
          <input type="submit" name="newmartyr_submit" value="       ثبت      ">
        </td>
      </tr>
    </table>
  </form>
<?php
}
//************************** NEW MARTYR FORM - end

//************************** EDIT MARTYR FORM
function edit_martyr_form($cc, $e_martyr_id)
{
  global $PHP_SELF;
  if (file_exists(TMP_PIC_DIR.TMP_PIC))
    unlink(TMP_PIC_DIR.TMP_PIC);
  $table = 'martyrs';
  $query = 'select * from '.$table.' a where '.
    'a.id='.$e_martyr_id;
  $e_martyr =  mysql_query($query);
  $num = mysql_num_rows($e_martyr);
  if ($num == 0)
    echo 'رکوردی با این شماره پیدا نشد.';
  else
  {
    $e_martyr1 = mysql_fetch_object($e_martyr);
  ?>
    <form method="post"
      action="<? echo $PHP_SELF; ?>?cc=<? echo $cc; ?>&id=<?php echo $e_martyr_id; ?>">
      <table class="tform" align="center">
        <tr>
          <td><label>نام:</label></td>
          <td>
            <input type="text" name="martyr_name" maxlength="20"
              value="<? echo $e_martyr1->name; ?>">
            </td>
          <td><label>نام&nbsp;خانوادگی:</label></td>
          <td>
            <input type="text" name="martyr_family" maxlength="30"
                value="<? echo $e_martyr1->family; ?>">
              </td>
          <td><label>نام&nbsp;پدر:</label></td>
          <td>
              <input type="text" name="martyr_faname" maxlength="20"
              value="<? echo $e_martyr1->faname; ?>">
            </td>
        </tr>
        <tr>
          <td><label>محل&nbsp;تولد:</label></td>
          <td>
            <input type="text" name="martyr_pbirth" maxlength="20" 
              value="<? echo $e_martyr1->pbirth; ?>">
          </td>
          <td><label>تاریخ&nbsp;تولد:</label></td>
          <td>
            <span dir="ltr">
              <input type="text" name="martyr_bdate" maxlength="10"
                  value="<? echo $e_martyr1->bdate; ?>">
            </span>
          </td>
          <td colspan="2">
            <input type="button" name="upload_image" value=" بارگذاري عكس " 
              onClick="upload_win=window.open('upload.php?tdir=<? 
              echo TMP_PIC_DIR; ?>', 'upload_win', 'toolbar=no');">
          <? if ($cc == 1) {
          echo '<br>';
          $eid = get_entry_id(get_table_id($table), $e_martyr1->id);
          $albumq = 'select id, pid from album a where a.pid='.$eid.
            ' and a.prim=1';
          $albumq = mysql_query($albumq);
          if (mysql_num_rows($albumq))
          {
            $albumq = mysql_fetch_object($albumq);
            echo '<img src="'.MARTYRS_PIC_DIR.$eid.'-'.$albumq->id.
              '.jpg" width="75px" onclick="showpic_win=window.open(\'showpic.php?pic='.
              MARTYRS_PIC_DIR.$eid.'-'.$albumq->id.'.jpg\', \'showpic_win\', '.
              '\'toolbar=no\');" align="right">';
          }
          ?>
          در صورت بارگذاري عكس جديد، عكس قبلي از بين مي‌رود.<br>
          <input type="checkbox" name="pic_delete">حذف تصوير
          <? } ?>
          </td>
        </tr>
        <?php
        if ($cc == 2)
        {
        ?>
        <tr>
          <td><label>محل&nbsp;شهادت:</label></td>
          <td>
            <input type="text" name="martyr_pmartyrdom" maxlength="50"
              value="<? echo $e_martyr1->pmartyrdom; ?>">
          </td>
          <td><label>تاریخ&nbsp;شهادت:</label></td>
          <td>
            <span dir="ltr">
              <input type="text" name="martyr_mdate" maxlength="10"
                  value="<? echo $e_martyr1->mdate; ?>">
            </span>
          </td>
          <td colspan="2" style="width:10%; text-align:right">
          <?
          $eid = get_entry_id(get_table_id($table), $e_martyr1->id);
          $albumq = 'select id, pid from album a where a.pid='.$eid.
            ' and a.prim=1';
          $albumq = mysql_query($albumq);
          if (mysql_num_rows($albumq))
          {
            $albumq = mysql_fetch_object($albumq);
            echo '<img src="'.MARTYRS_PIC_DIR.$eid.'-'.$albumq->id.
              '.jpg" width="75px" onclick="showpic_win=window.open(\'showpic.php?pic='.
              MARTYRS_PIC_DIR.$eid.'-'.$albumq->id.'.jpg\', \'showpic_win\', '.
              '\'toolbar=no\');" align="right">';
          }
          ?>
          در صورت بارگذاري عكس جديد، عكس قبلي از بين مي‌رود.<br>
          <input type="checkbox" name="pic_delete">حذف تصوير
          </td>
        </tr>
        <? } ?>
        <tr>
          <td colspan="6">
            <label>زندگی‌نامه:</label><br>
            <textarea name="martyr_bio"><? echo $e_martyr1->bio; ?></textarea>
          </td>
        </tr>
        <?php
        if ($cc == 2)
        {
        ?>
        <tr>
          <td colspan="6">
            <label>وصیت‌نامه:</label><br>
            <textarea name="martyr_testament"><? echo $e_martyr1->testament; ?></textarea>
          </td>
        </tr>
        <? } ?>
        <tr>
          <td colspan="6">
            <label>انتشار بیابد؟</label>
            <?php
            if ($e_martyr1->publish == 1) 
              echo '<input type="checkbox" name="martyr_publish" checked>';
            else
              echo '<input type="checkbox" name="martyr_publish">';
            ?>
          </td>
        </tr>
        <tr>
          <td colspan="6" align="center">
            <input type="submit" name="editmartyr_submit" value="  ثبت تغییرات  ">
          </td>
        </tr>
      </table>
    </form>
<?php
  }
}
//************************** EDIT MARTYR FORM - end

//************************** NEW ASAR FORM
function new_asar_form()
{
  global $PHP_SELF;
  if (file_exists(TMP_PIC_DIR.TMP_PIC))
    unlink(TMP_PIC_DIR.TMP_PIC);
?>
  <form method="post" action="<? echo $PHP_SELF; ?>">
    <table class="tform" align="center">
      <tr>
        <td>
          <label>عنوان:</label>
            <input type="text" name="asar_title" maxlength="100">
        </td>
        <td>
        <?php
        $apid = 'select id, title from martyrs';
        $apid = mysql_query($apid);
        $apidno = mysql_num_rows($apid);
        ?>
          <label>اثر مربوط به كيست؟</label>
          <select name="asar_pid">
            <option value="0">&nbsp;</option>
            <?php
            for ($i = 0; $i < $apidno; $i++)
            {
              $apid1 = mysql_fetch_object($apid);
              ?>
              <option value="<? echo $apid1->id; ?>">
                <? echo $apid1->title; ?>
              </option>
              <?
            }
            ?>
          </select>
        </td>
        <td>
          <input type="button" name="upload_image" value=" بارگذاري عكس " 
            onClick="upload_win=window.open('upload.php?tdir=<? 
            echo TMP_PIC_DIR; ?>', 'upload_win', 'toolbar=no');">
        </td>
      </tr>
      <tr>
        <td colspan="3">
          <label>متن:</label><br>
          <textarea name="asar_text"></textarea>
        </td>
      </tr>
      <tr>
        <td colspan="3">
          <label>انتشار بیابد؟</label>
          <input type="checkbox" name="asar_publish">
        </td>
      </tr>
      <tr>
        <td colspan="3" align="center">
          <input type="submit" name="newasar_submit" value="       ثبت      ">
        </td>
      </tr>
    </table>
  </form>
<?
}
//************************** NEW ASAR FORM - end

//************************** EDIT ASAR FORM
function edit_asar_form($e_asar_id)
{
  global $PHP_SELF;
  if (file_exists(TMP_PIC_DIR.TMP_PIC))
    unlink(TMP_PIC_DIR.TMP_PIC);
  $table = 'asar';
  $query = 'select * from '.$table.' a where '.
    'a.id='.$e_asar_id;
  $e_asar =  mysql_query($query);
  $num = mysql_num_rows($e_asar);
  if ($num == 0)
    echo 'رکوردی با این شماره پیدا نشد.';
  else
  {
    $e_asar1 = mysql_fetch_object($e_asar);
    ?>
    <form method="post" action="<? echo $PHP_SELF; ?>?id=<?php echo $e_asar_id; ?>">
      <table class="tform" align="center">
        <tr>
          <td>
            <label>عنوان:</label>
            <input type="text" name="asar_title" maxlength="100"
              value="<? echo $e_asar1->title; ?>">
          </td>
          <td>
            <?php
            $apid = 'select id, title from martyrs';
            $apid = mysql_query($apid);
            $apidno = mysql_num_rows($apid);
            ?>
            <select name="asar_pid">
              <option value="0">&nbsp;</option>
              <?php
              for ($i = 0; $i < $apidno; $i++)
              {
                $apid1 = mysql_fetch_object($apid);
                if ($apid1->id == $e_asar1->pid)
                  $selected = ' selected';
                else
                  $selected = '';
                ?>
                <option value="<? echo $apid1->id; ?>"<? echo $selected; ?>>
                  <? echo $apid1->title; ?>
                </option>
                <?
              }
              ?>
            </select>
          </td>
        </tr>
      <tr>
        <td colspan="2">
          <input type="button" name="upload_image" value=" بارگذاري عكس " 
            onClick="upload_win=window.open('upload.php?tdir=<? 
            echo TMP_PIC_DIR; ?>', 'upload_win', 'toolbar=no');">
          <br>
          <?
          $eid = get_entry_id(get_table_id($table), $e_asar1->id);
          $albumq = 'select id, pid from album a where a.pid='.$eid.
            ' and a.prim=1';
          $albumq = mysql_query($albumq);
          if (mysql_num_rows($albumq))
          {
            $albumq = mysql_fetch_object($albumq);
            echo '<img src="'.ASAR_PIC_DIR.$eid.'-'.$albumq->id.
              '.jpg" width="75px" onclick="showpic_win=window.open(\'showpic.php?pic='.
              ASAR_PIC_DIR.$eid.'-'.$albumq->id.'.jpg\', \'showpic_win\', '.
              '\'toolbar=no\');" align="right">';
          }
          ?>
          در صورت بارگذاري عكس جديد، عكس قبلي از بين مي‌رود.<br>
          <input type="checkbox" name="pic_delete">حذف تصوير
        </td>
      </tr>
        <tr>
          <td colspan="2">
            <label>متن:</label><br>
            <textarea name="asar_text"><? echo $e_asar1->text; ?></textarea>
          </td>
        </tr>
        <tr>
          <td colspan="3">
            <label>انتشار بیابد؟</label>
            <?php
            if ($e_asar1->publish == 1) 
              echo '<input type="checkbox" name="asar_publish" checked>';
            else
              echo '<input type="checkbox" name="asar_publish">';
            ?>
          </td>
        </tr>
        <tr>
          <td colspan="2" align="center">
            <input type="submit" name="editasar_submit" value="  ثبت تغییرات  ">
          </td>
        </tr>
      </table>
    </form>
  <?php
  }
}
//************************** EDIT ASAR FORM - end

//************************** SET ETC
function set_etc($t, $n)
{
  if (strlen($t) > $n)
    return '...';
  else
    return '';
}
//************************** SET ETC - end

//************************** NEW NEWS FORM
function new_news_form()
{
  global $PHP_SELF;
  if (file_exists(TMP_PIC_DIR.TMP_PIC))
    unlink(TMP_PIC_DIR.TMP_PIC);
  ?>
  <form method="post" action="<? echo $PHP_SELF; ?>">
    <table class="tform" align="center">
      <tr>
        <td>
          <label>عنوان:</label>
          <input type="text" name="news_title" maxlength="100">
        </td>
        <td>
          <label>تاریخ:</label>
          <span dir="ltr">
            <input type="text" name="news_ndate" maxlength="10" value="<? echo mydate(); ?>">
          </span>
        </td>
        <td>
          <input type="button" name="upload_image" value=" بارگذاري عكس " 
            onClick="upload_win=window.open('upload.php?tdir=<? 
            echo TMP_PIC_DIR; ?>', 'upload_win', 'toolbar=no');">
        </td>
      </tr>
      <tr>
        <td colspan="3">
          <label>خلاصه:</label><br>
          <textarea name="news_summary" style="height:200px"></textarea>
        </td>
      </tr>
      <tr>
        <td colspan="3">
          <label>متن:</label><br>
          <textarea name="news_text"></textarea>
        </td>
      </tr>
      <tr>
        <td colspan="3">
          <label>انتشار بیابد؟</label>
          <input type="checkbox" name="news_publish">
        </td>
      </tr>
      <tr>
        <td colspan="3" align="center">
          <input type="submit" name="newnews_submit" value="     ثبت     ">
        </td>
      </tr>
    </table>
  </form>
<?
}
//************************** NEW NEWS FORM - end

//************************** EDIT NEWS FORM
function edit_news_form($e_news_id)
{
  global $PHP_SELF;
  if (file_exists(TMP_PIC_DIR.TMP_PIC))
    unlink(TMP_PIC_DIR.TMP_PIC);
  $table = 'news';
  $query = 'select * from '.$table.' a where '.
    'a.id='.$e_news_id;
  $e_news =  mysql_query($query);
  $num = mysql_num_rows($e_news);
  if ($num == 0)
    echo 'رکوردی با این شماره پیدا نشد.';
  else
  {
    $e_news1 = mysql_fetch_object($e_news);
    ?>
    <form method="post" action="<? echo $PHP_SELF; ?>?id=<?php echo $e_news_id; ?>">
    <table class="tform" align="center">
      <tr>
        <td>
          <label>عنوان:</label>
          <input type="text" name="news_title" maxlength="100"
            value="<? echo $e_news1->title; ?>">
        </td>
        <td>
          <label>تاريخ:</label>
          <span dir="ltr">
            <input type="text" name="news_ndate" maxlength="10"
              value="<? echo $e_news1->ndate; ?>">
          </span>
        </td>
      <tr>
        <td colspan="2">
          <input type="button" name="upload_image" value=" بارگذاري عكس " 
            onClick="upload_win=window.open('upload.php?tdir=<? 
            echo TMP_PIC_DIR; ?>', 'upload_win', 'toolbar=no');">
          <br>
          <?
          $eid = get_entry_id(get_table_id($table), $e_news1->id);
          $albumq = 'select id, pid from album a where a.pid='.$eid.
            ' and a.prim=1';
          $albumq = mysql_query($albumq);
          if (mysql_num_rows($albumq))
          {
            $albumq = mysql_fetch_object($albumq);
            echo '<img src="'.NEWS_PIC_DIR.$eid.'-'.$albumq->id.
              '.jpg" width="75px" onclick="showpic_win=window.open(\'showpic.php?pic='.
              NEWS_PIC_DIR.$eid.'-'.$albumq->id.'.jpg\', \'showpic_win\', '.
              '\'toolbar=no\');" align="right">';
          }
          ?>
          در صورت بارگذاري عكس جديد، عكس قبلي از بين مي‌رود.<br>
          <input type="checkbox" name="pic_delete">حذف تصوير
        </td>
      </tr>
      <tr>
        <td colspan="2">
          <label>خلاصه:</label><br>
          <textarea name="news_summary" style="height:200px"><? echo $e_news1->summary; ?></textarea>
        </td>
      </tr>
      <tr>
        <td colspan="2">
          <label>متن:</label><br>
          <textarea name="news_text"><? echo $e_news1->text; ?></textarea>
        </td>
      </tr>
      <tr>
        <td colspan="3">
          <label>انتشار بیابد؟</label>
          <?php
          if ($e_news1->publish == 1) 
            echo '<input type="checkbox" name="news_publish" checked>';
          else
            echo '<input type="checkbox" name="news_publish">';
          ?>
        </td>
      </tr>
      <tr>
        <td colspan="3" align="center">
          <input type="submit" name="editnews_submit" value="  ثبت تغییرات  ">
        </td>
      </tr>
    </table>
  </form>
  <?php
  }
}
//************************** EDIT NEWS FORM - end

//************************** NEW MCATEGORY
function new_mcategory_form()
{
  global $PHP_SELF;
  ?>
  <form method="post" action="<? echo $PHP_SELF; ?>">
    <table class="tform" align="center">
      <tr>
        <td>
          <label>عنوان:</label>
          <input type="text" name="mcategory_title" maxlength="20">
        </td>
      </tr>
      <tr>
        <td colspan="3">
          <label>توضیح:</label><br>
          <textarea name="mcategory_description"></textarea>
        </td>
      </tr>
      <tr>
        <td colspan="3" align="center">
          <input type="submit" name="newmcategory_submit" value="       ثبت      ">
        </td>
      </tr>
    </table>
  </form>
<?
}
//************************** NEW MCATEGORY - end

//************************** EDIT MCATEGORY FORM
function edit_mcategory_form($e_mcategory_id)
{
  global $PHP_SELF;
  $table = 'mcategories';
  $query = 'select * from '.$table.' a where '.
    'a.id='.$e_mcategory_id;
  $e_mcategory =  mysql_query($query);
  $num = mysql_num_rows($e_mcategory);
  if ($num == 0)
    echo 'رکوردی با این شماره پیدا نشد.';
  else
  {
    $e_mcategory1 = mysql_fetch_object($e_mcategory);
    ?>
    <form method="post" action="<? echo $PHP_SELF; ?>?id=<?php echo $e_mcategory_id; ?>">
      <table class="tform" align="center">
        <tr>
          <td>
            <label>عنوان:</label>
            <input type="text" name="mcategory_title" maxlength="20"
              value="<? echo $e_mcategory1->title; ?>">
          </td>
        <tr>
          <td colspan="3">
            <label>توضیح:</label><br>
<textarea name="mcategory_description"><? echo $e_mcategory1->description; ?></textarea>
          </td>
        </tr>
        <tr>
          <td colspan="3" align="center">
            <input type="submit" name="editmcategory_submit" value="  ثبت تغییرات  ">
          </td>
        </tr>
      </table>
    </form>
  <?php
  }
}
//************************** EDIT MCATEGORY FORM - end

//************************** NEW LCATEGORY FORM
function new_lcategory_form()
{
  global $PHP_SELF;
  ?>
  <form method="post" action="<? echo $PHP_SELF; ?>">
    <table class="tform" align="center">
      <tr>
        <td>
          <label>عنوان:</label>
          <input type="text" name="lcategory_title" maxlength="20">
        </td>
      </tr>
      <tr>
        <td>
          <label>توضیح:</label><br>
          <textarea name="lcategory_description"></textarea>
        </td>
      </tr>
      <tr>
        <td align="center">
          <input type="submit" name="newlcategory_submit" value="       ثبت      ">
        </td>
      </tr>
    </table>
  </form>
  <?
}
//************************** NEW LCATEGORY FORM - end

//************************** EDIT LCATEGORY FORM
function edit_lcategory_form($e_lcategory_id)
{
  global $PHP_SELF;
  $table = 'lcategories';
  $query = 'select * from '.$table.' a where '.
    'a.id='.$e_lcategory_id;
  $e_lcategory =  mysql_query($query);
  $num = mysql_num_rows($e_lcategory);
  if ($num == 0)
    echo 'رکوردی با این شماره پیدا نشد.';
  else
  {
    $e_lcategory1 = mysql_fetch_object($e_lcategory);
    ?>
    <form method="post" action="<? echo $PHP_SELF; ?>?id=<?php echo $e_lcategory_id; ?>">
      <table class="tform" align="center">
        <tr>
          <td>
            <label>عنوان:</label>
            <input type="text" name="lcategory_title" maxlength="20"
              value="<? echo $e_lcategory1->title; ?>">
          </td>
        <tr>
          <td>
            <label>توضیح:</label><br>
<textarea name="lcategory_description" rows="5" cols="50"><? echo $e_lcategory1->description; ?></textarea>
          </td>
        </tr>
        <tr>
          <td align="center">
            <input type="submit" name="editlcategory_submit" value="  ثبت تغییرات  ">
          </td>
        </tr>
      </table>
    </form>
    <?php
  }
}
//************************** EDIT LCATEGORY FORM - end

//************************** NEW MTEXT FORM
function new_mtext_form()
{
  global $PHP_SELF;
  ?>
  <form method="post" action="<? echo $PHP_SELF; ?>">
    <table class="tform" align="center">
      <tr>
        <td>
          <label>عنوان:</label>
          <input type="text" name="mtext_title" maxlength="100">
        </td>
        <td>
          <label>دسته:</label>
          <select name="mtext_ccode">
            <option value="0">&nbsp;</option>
            <?php
            $ccodes = 'select id, title from mcategories';
            $ccodes = mysql_query($ccodes);
            $ccno = mysql_num_rows($ccodes);
            for ($i = 1; $i <= $ccno; $i++)
            {
              $cc1 = mysql_fetch_object($ccodes);
              echo '<option value="'.$cc1->id.'">'.$cc1->title.'</option>'."\n";
            }
            ?>
          </select>
        </td>
      </tr>
      <tr>
        <td colspan="2">
          <label>متن:</label><br>
          <textarea name="mtext_text"></textarea>
        </td>
      </tr>
      <tr>
        <td colspan="2">
          <label>نویسنده:</label>
          <input type="text" name="mtext_author" maxlength="50">
        </td>
      </tr>
      <tr>
        <td colspan="2">
          <label>انتشار بیابد؟</label>
          <input type="checkbox" name="mtext_publish">
        </td>
      </tr>
      <tr>
        <td colspan="2" align="center">
          <input type="submit" name="newmtext_submit" value="       ثبت      ">
        </td>
      </tr>
    </table>
  </form>
  <?
}
//************************** NEW MTEXT FORM - end

//************************** EDIT MTEXT FORM
function edit_mtext_form($e_mtext_id)
{
  global $PHP_SELF;
  $table = 'mtexts';
  $query = 'select * from '.$table.' a where '.
    'a.id='.$e_mtext_id;
  $e_mtext =  mysql_query($query);
  $num = mysql_num_rows($e_mtext);
  if ($num == 0)
    echo 'رکوردی با این شماره پیدا نشد.';
  else
  {
    $e_mtext1 = mysql_fetch_object($e_mtext);
    ?>
    <form method="post" action="<? echo $PHP_SELF; ?>?id=<?php echo $e_mtext_id; ?>">
      <table class="tform" align="center">
        <tr>
          <td>
            <label>عنوان:</label>
            <input type="text" name="mtext_title" maxlength="200"
              value="<? echo $e_mtext1->title; ?>">
          </td>
          <td>
            <label>دسته:</label>
            <select name="mtext_ccode">
              <option value="0">&nbsp;</option>
              <?php
              $ccodes = 'select id, title from mcategories';
              $ccodes = mysql_query($ccodes);
              $ccno = mysql_num_rows($ccodes);
              for ($i = 1; $i <= $ccno; $i++)
              {
                $cc1 = mysql_fetch_object($ccodes);
                if ($e_mtext1->ccode == $cc1->id)
                  $selected = ' selected';
                else
                  $selected = '';
                echo '<option value="'.$cc1->id.'"'.$selected.'>'.
                  $cc1->title.'</option>'."\n";
              }
              ?>
            </select>
          </td>
        <tr>
          <td colspan="2">
            <label>متن:</label><br>
       <textarea name="mtext_text" rows="5" cols="50"><? echo $e_mtext1->text; ?></textarea>
          </td>
        </tr>
        <tr>
          <td colspan="2">
            <label>نویسنده:</label>
            <input type="text" name="mtext_author" maxlength="100"
              value="<? echo $e_mtext1->author; ?>">
          </td>
        </tr>
        <tr>
          <td colspan="2">
            <label>انتشار بیابد؟</label>
            <?php
            if ($e_mtext1->publish == 1) 
              echo '<input type="checkbox" name="mtext_publish" checked>';
            else
              echo '<input type="checkbox" name="mtext_publish">';
            ?>
          </td>
        </tr>
        <tr>
          <td colspan="2" align="center">
            <input type="submit" name="editmtext_submit" value="  ثبت تغییرات  ">
          </td>
        </tr>
      </table>
    </form>
    <?php
  }
}
//************************** EDIT MTEXT FORM - end

//************************** NEW LTEXT FORM
function new_ltext_form()
{
  global $PHP_SELF;
  ?>
  <form method="post" action="<? echo $PHP_SELF; ?>">
    <table class="tform" align="center">
      <tr>
        <td>
          <label>عنوان:</label>
          <input type="text" name="ltext_title" maxlength="100">
        </td>
        <td>
          <label>دسته:</label>
          <select name="ltext_ccode">
            <option value="0">&nbsp;</option>
            <?php
            $ccodes = 'select id, title from lcategories';
            $ccodes = mysql_query($ccodes);
            $ccno = mysql_num_rows($ccodes);
            for ($i = 1; $i <= $ccno; $i++)
            {
              $cc1 = mysql_fetch_object($ccodes);
              echo '<option value="'.$cc1->id.'">'.$cc1->title.'</option>'."\n";
            }
            ?>
          </select>
        </td>
      </tr>
      <tr>
        <td colspan="2">
          <label>متن:</label><br>
          <textarea name="ltext_text" rows="5" cols="50"></textarea>
        </td>
      </tr>
      <tr>
        <td>
          <label>نویسنده:</label>
          <input type="text" name="ltext_author" maxlength="50">
        </td>
      </tr>
      <tr>
        <td colspan="2">
          <label>انتشار بیابد؟</label>
          <input type="checkbox" name="ltext_publish">
        </td>
      </tr>
      <tr>
        <td colspan="2" align="center">
          <input type="submit" name="newltext_submit" value="       ثبت      ">
        </td>
      </tr>
    </table>
  </form>
  <?
}
//************************** NEW LTEXT FORM - end

//************************** EDIT LTEXT FORM
function edit_ltext_form($e_ltext_id)
{
  global $PHP_SELF;
  $table = 'ltexts';
  $query = 'select * from ltexts a where '.
    'a.id='.$e_ltext_id;
  $e_ltext =  mysql_query($query);
  $num = mysql_num_rows($e_ltext);
  if ($num == 0)
    echo 'رکوردی با این شماره پیدا نشد.';
  else
  {
    $e_ltext1 = mysql_fetch_object($e_ltext);
    ?>
    <form method="post" action="<? echo $PHP_SELF; ?>?id=<?php echo $e_ltext_id; ?>">
      <table class="tform" align="center">
        <tr>
          <td>
            <label>عنوان:</label>
            <input type="text" name="ltext_title" maxlength="100"
              value="<? echo $e_ltext1->title; ?>">
          </td>
          <td>
            <label>دسته:</label>
            <select name="ltext_ccode">
              <option value="0">&nbsp;</option>
              <?php
              $ccodes = 'select id, title from lcategories';
              $ccodes = mysql_query($ccodes);
              $ccno = mysql_num_rows($ccodes);
              for ($i = 1; $i <= $ccno; $i++)
              {
                $cc1 = mysql_fetch_object($ccodes);
                if ($e_ltext1->ccode == $cc1->id)
                  $selected = ' selected';
                else
                  $selected = '';
                echo '<option value="'.$cc1->id.'"'.$selected.'>'.
                  $cc1->title.'</option>'."\n";
              }
              ?>
            </select>
          </td>
        <tr>
          <td colspan="2">
            <label>متن:</label><br>
   <textarea name="ltext_text" rows="5" cols="50"><? echo $e_ltext1->text; ?></textarea>
          </td>
        </tr>
        <tr>
          <td>
            <label>نویسنده:</label>
            <input type="text" name="ltext_author" maxlength="100"
              value="<? echo $e_ltext1->author; ?>">
          </td>
        </tr>
        <tr>
          <td colspan="2">
            <label>انتشار بیابد؟</label>
            <?php
            if ($e_ltext1->publish == 1) 
              echo '<input type="checkbox" name="ltext_publish" checked>';
            else
              echo '<input type="checkbox" name="ltext_publish">';
            ?>
          </td>
        </tr>
        <tr>
          <td colspan="2" align="center">
            <input type="submit" name="editltext_submit" value="  ثبت تغییرات  ">
          </td>
        </tr>
      </table>
    </form>
    <?php
  }
}
//************************** EDIT LTEXT FORM - end

//************************** WI HI
function wi_hi($fullshow)
{
  if ($fullshow)
    return '';
  else
    return ' width="100" ';
}
//************************** WI HI - end

//************************** SHOW MARTYR
function show_martyr($id, $fullshow)
{
  global $main_url;
  $table = 'martyrs';
  $r = get_record($table, $id);
  $eid = get_entry_id(get_table_id($table), $r->id);
  ?>
  <table>
    <tr>
      <td style="border-bottom:dotted 1px #99CCCC">
        <?
        if ($r->ccode == 2)
        {
          $f1 = '<span style="color:red">';
          $f2 = '</span>';
          $tt = 'شهدا';
        }
        else
            if ($r->ccode == 1)
            {
              $f1 = '';
              $f2 = '';
              $tt = 'رزمندگان';
            }
        ?>
        <b><span style="background:#999999">
        <a href="s_martyrs.php?cc=<? echo $r->ccode; ?>"><? echo $tt; ?></a>
        </span></b>&nbsp;
        <b><a href="<? echo $main_url; ?>s_martyrs.php?cc=<? echo $r->ccode; ?>&id=<? echo $eid; ?>">
        <? echo $f1.substr($r->title, 0, 8).$f2.
          substr($r->title, 8); ?></a></b>
      </td>
    </tr>
    <tr>
      <td>
        <br>
        <?
        $albumq = 'select id, pid from album a where a.pid='.$eid.
          ' and a.prim=1';
        $albumq = mysql_query($albumq);
        if (mysql_num_rows($albumq))
        {
          $albumq = mysql_fetch_object($albumq);
          echo '<img src="'.MARTYRS_PIC_DIR.$eid.'-'.$albumq->id.'.jpg" align="left"'.
            wi_hi($fullshow).'>';
        }
        if ($fullshow)
        {
        ?>
        <span class="myh">زندگي‌نامه:</span>
        <? echo $r->bio;
        if ($r->ccode == 2)
        {
        ?>
        <div style="border-bottom:dotted 1px #99CCCC" noshade="noshade">
          &nbsp;
        </div>
        <span class="myh">وصيت‌نامه:</span>
        <? echo $r->testament;
        }

        }
        else
        {
        ?>
        <span class="myh">زندگي‌نامه:</span>
        <? echo substr($r->bio, 0, NSUMCHAR_BIG).
           set_etc($r->bio, NSUMCHAR_BIG);
        if ($r->ccode == 2)
        {
        ?>
        <div style="border-bottom:dotted 1px #99CCCC" noshade="noshade">
          &nbsp;
        </div>
        <span class="myh">وصيت‌نامه:</span>
        <? echo substr($r->testament, 0, NSUMCHAR_BIG).
          set_etc($r->testament, NSUMCHAR_BIG);
        }
          echo '<br><a href="'.$main_url.'s_martyrs.php?cc='.
            $r->ccode.'&id='.$eid.'">ادامه مطلب...</a>';
        }
        ?>
      </td>
    </tr>
  </table>
  <?
}
//************************** SHOW MARTYR - end

//************************** SHOW NEWS
function show_news($id, $fullshow)
{
  $table = 'news';
  $r = get_record($table, $id);
  $eid = get_entry_id(get_table_id($table), $r->id);
  ?>
  <table>
    <tr>
      <td>
        <b><span style="background:#999999; color:#FFCCCC;">
        <a href="s_news.php">خبر</a>
        </span></b>&nbsp;
        <a href="<? echo $main_url; ?>s_news.php?id=<? 
          echo get_entry_id(get_table_id($table), $r->id); ?>">
        <b><? echo $r->title; ?></b></a>
      </td>
    </tr>
    <tr>
      <td>
        <br>
        <?
        $albumq = 'select id, pid from album a where a.pid='.$eid.
          ' and a.prim=1';
        $albumq = mysql_query($albumq);
        if (mysql_num_rows($albumq))
        {
          $albumq = mysql_fetch_object($albumq);
          echo '<img src="'.NEWS_PIC_DIR.$eid.'-'.$albumq->id.'.jpg" align="left"'.
            wi_hi($fullshow).'>';
        }
        if ($fullshow)
        {
          echo $r->text.'<br><span dir="ltr">'.$r->ndate.'</span>';
        }
        else
        {
        if ($r->summary)
          $news = $r->summary;
        else
          $news = substr($r->text, 0, NSUMCHAR_BIG).set_etc($r->text, NSUMCHAR_BIG);
        echo $news.'<br><span dir="ltr">'.$r->ndate.'</span>';
          echo '<br><a href="'.$main_url.'s_news.php?id='.
            get_entry_id(get_table_id($table), $r->id).
            '">ادامه مطلب...</a>';
        }
        ?>
      </td>
    </tr>
  </table>
  <?
}
//************************** SHOW NEWS - end

//************************** SHOW MTEXT
function show_mtext($id, $fullshow)
{
  //global NSUMCHAR_BIG;
  $table = 'mtexts';
  $r = get_record($table, $id);
  ?>
  <table>
    <tr>
      <td>
        <b><span style="background:#999999; color:#FFCCCC;">
        <a href="s_martyrdom.php?cc=<? echo $r->ccode; ?>">
        <? echo get_category($r->ccode); ?></a>
        </span></b>&nbsp;
        <a href="<? echo $main_url; ?>s_martyrdom.php?cc=<?
          echo $r->ccode; ?>&id=<? 
          echo get_entry_id(get_table_id($table), $r->id); ?>">
        <b><? echo $r->title; ?></b></a>
      </td>
    </tr>
    <tr>
      <td>
        <?
        if ($fullshow)
        {
          echo $r->text;
        }
        else
        {
          echo substr($r->text, 0, NSUMCHAR_BIG).set_etc($r->text, NSUMCHAR_BIG);
          echo '<br><a href="'.$main_url.'s_martyrdom.php?cc='.$r->ccode.'&id='.
            get_entry_id(get_table_id($table), $r->id).
            '">ادامه مطلب...</a>';
        }
        ?>
      </td>
    </tr>
  </table>
  <?
}
//************************** SHOW MTEXT - end

//************************** SHOW LTEXT
function show_ltext($id, $fullshow)
{
  //global NSUMCHAR_BIG;
  $table = 'ltexts';
  $r = get_record($table, $id);
  ?>
  <table>
    <tr>
      <td>
        <b><span style="background:#999999; color:#FFCCCC;">
        <a href="s_moqavemat.php?cc=<? echo $r->ccode; ?>">
        <? echo get_category($r->ccode, 'l'); ?></a>
        </span></b>&nbsp;
        <a href="<? echo $main_url; ?>s_moqavemat.php?cc=<?
          echo $r->ccode; ?>&id=<? 
          echo get_entry_id(get_table_id($table), $r->id); ?>">
        <b><? echo $r->title; ?></b></a>
      </td>
    </tr>
    <tr>
      <td>
        <?
        if ($fullshow)
        {
          echo $r->text;
        }
        else
        {
          echo substr($r->text, 0, NSUMCHAR_BIG).set_etc($r->text, NSUMCHAR_BIG);
          echo '<br><a href="'.$main_url.'s_moqavemat.php?cc='.$r->ccode.'&id='.
            get_entry_id(get_table_id($table), $r->id).
            '">ادامه مطلب...</a>';
        }
        ?>
      </td>
    </tr>
  </table>
  <?
}
//************************** SHOW LTEXT - end

//************************** SHOW ASAR
function show_asar($id, $fullshow)
{
  //global NSUMCHAR_BIG;
  $table = 'asar';
  $r = get_record($table, $id);
  $eid = get_entry_id(get_table_id($table), $r->id);
  ?>
  <table>
    <tr>
      <td>
        <b><span style="background:#999999; color:#FFCCCC;">
        <a href="s_asar.php">آثار</a>
        </span></b>&nbsp;
        <a href="<? echo $main_url; ?>s_asar.php?id=<? 
          echo get_entry_id(get_table_id($table), $r->id); ?>">
        <b><? echo $r->title; ?></b></a>
      </td>
    </tr>
    <tr>
      <td>
        <br>
        <?
        $albumq = 'select id, pid from album a where a.pid='.$eid.
          ' and a.prim=1';
        $albumq = mysql_query($albumq);
        if (mysql_num_rows($albumq))
        {
          $albumq = mysql_fetch_object($albumq);
          echo '<img src="'.ASAR_PIC_DIR.$eid.'-'.$albumq->id.'.jpg" align="left"'.
            wi_hi($fullshow).'>';
        }
        if ($fullshow)
        {
          echo get_personal_title($r->pid).'<br>';
          echo $r->text;
        }
        else
        {
          echo substr($r->text, 0, NSUMCHAR_BIG).set_etc($r->text, NSUMCHAR_BIG);
          echo '<br><a href="'.$main_url.'s_asar.php?id='.
            get_entry_id(get_table_id($table), $r->id).
            '">ادامه مطلب...</a>';
        }
        ?>
      </td>
    </tr>
  </table>
  <?
}
//************************** SHOW ASAR - end

//************************** UPLOAD FORM
function upload_form($target_dir, $tmp = 0)
{
  global $PHP_SELF;
 ?>
  <form method="post" enctype="multipart/form-data"
    action="<? echo $PHP_SELF ?>?tdir=<? echo $target_dir; ?>&tmp=<? echo $tmp; ?>">
    بارگذاري فايل:
    <input type="file" name="userfile">
    <input type="submit" name="upload_submit" value="بارگذاري">
  </form>
  <?php
}
//************************** UPLOAD FORM - end

//************************** NEW REMEMBRANCE FORM
function new_remembrance_form()
{
  global $PHP_SELF;
  if (file_exists(TMP_PIC_DIR.TMP_PIC))
    unlink(TMP_PIC_DIR.TMP_PIC);
?>
  <form method="post" action="<? echo $PHP_SELF; ?>">
    <table class="tform" align="center">
      <tr>
        <td>
          <label>عنوان:</label>
            <input type="text" name="remembrance_title" maxlength="100">
        </td>
        <td>
        <?php
        $apid = 'select id, title from martyrs';
        $apid = mysql_query($apid);
        $apidno = mysql_num_rows($apid);
        ?>
          <label>خاطره مربوط به كيست؟</label>
          <select name="remembrance_pid">
            <option value="0">&nbsp;</option>
            <?php
            for ($i = 0; $i < $apidno; $i++)
            {
              $apid1 = mysql_fetch_object($apid);
              ?>
              <option value="<? echo $apid1->id; ?>">
                <? echo $apid1->title; ?>
              </option>
              <?
            }
            ?>
          </select>
        </td>
        <td>
          <input type="button" name="upload_image" value=" بارگذاري عكس " 
            onClick="upload_win=window.open('upload.php?tdir=<? 
            echo TMP_PIC_DIR; ?>', 'upload_win', 'toolbar=no');">
        </td>
      </tr>
      <tr>
        <td colspan="3">
          <label>متن:</label><br>
          <textarea name="remembrance_text"></textarea>
        </td>
      </tr>
      <tr>
        <td colspan="3">
          <label>انتشار بیابد؟</label>
          <input type="checkbox" name="remembrance_publish">
        </td>
      </tr>
      <tr>
        <td colspan="3" align="center">
          <input type="submit" name="newremembrance_submit" value="       ثبت      ">
        </td>
      </tr>
    </table>
  </form>
<?
}
//************************** NEW REMEMBRANCE FORM - end

//************************** EDIT REMEMBRANCE FORM
function edit_remembrance_form($e_remembrance_id)
{
  global $PHP_SELF;
  if (file_exists(TMP_PIC_DIR.TMP_PIC))
    unlink(TMP_PIC_DIR.TMP_PIC);
  $table = 'remembrances';
  $query = 'select * from '.$table.' a where '.
    'a.id='.$e_remembrance_id;
  $e_remembrances =  mysql_query($query);
  $num = mysql_num_rows($e_remembrances);
  if ($num == 0)
    echo 'رکوردی با این شماره پیدا نشد.';
  else
  {
    $e_remembrance1 = mysql_fetch_object($e_remembrances);
    ?>
    <form method="post" action="<? echo $PHP_SELF; ?>?id=<?php echo $e_remembrance_id; ?>">
      <table class="tform" align="center">
        <tr>
          <td>
            <label>عنوان:</label>
            <input type="text" name="remembrance_title" maxlength="100"
              value="<? echo $e_remembrance1->title; ?>">
          </td>
          <td>
            <?php
            $apid = 'select id, title from martyrs';
            $apid = mysql_query($apid);
            $apidno = mysql_num_rows($apid);
            ?>
            <select name="remembrance_pid">
              <option value="0">&nbsp;</option>
              <?php
              for ($i = 0; $i < $apidno; $i++)
              {
                $apid1 = mysql_fetch_object($apid);
                if ($apid1->id == $e_remembrance1->pid)
                  $selected = ' selected';
                else
                  $selected = '';
                ?>
                <option value="<? echo $apid1->id; ?>"<? echo $selected; ?>>
                  <? echo $apid1->title; ?>
                </option>
                <?
              }
              ?>
            </select>
          </td>
        </tr>
      <tr>
        <td colspan="2">
          <input type="button" name="upload_image" value=" بارگذاري عكس " 
            onClick="upload_win=window.open('upload.php?tdir=<? 
            echo TMP_PIC_DIR; ?>', 'upload_win', 'toolbar=no');">
          <br>
          <?
          $eid = get_entry_id(get_table_id($table), $e_remembrance1->id);
          $albumq = 'select id, pid from album a where a.pid='.$eid.
            ' and a.prim=1';
          $albumq = mysql_query($albumq);
          if (mysql_num_rows($albumq))
          {
            $albumq = mysql_fetch_object($albumq);
            echo '<img src="'.REMEMBRANCES_PIC_DIR.$eid.'-'.$albumq->id.
              '.jpg" width="75px" onclick="showpic_win=window.open(\'showpic.php?pic='.
              REMEMBRANCES_PIC_DIR.$eid.'-'.$albumq->id.'.jpg\', \'showpic_win\', '.
              '\'toolbar=no\');" align="right">';
          }
          ?>
          در صورت بارگذاري عكس جديد، عكس قبلي از بين مي‌رود.<br>
          <input type="checkbox" name="pic_delete">حذف تصوير
        </td>
      </tr>
        <tr>
          <td colspan="2">
            <label>متن:</label><br>
            <textarea name="remembrance_text"><? echo $e_remembrance1->text; ?></textarea>
          </td>
        </tr>
        <tr>
          <td colspan="3">
            <label>انتشار بیابد؟</label>
            <?php
            if ($e_remembrance1->publish == 1) 
              echo '<input type="checkbox" name="remembrance_publish" checked>';
            else
              echo '<input type="checkbox" name="remembrance_publish">';
            ?>
          </td>
        </tr>
        <tr>
          <td colspan="2" align="center">
            <input type="submit" name="editremembrance_submit" value="  ثبت تغییرات  ">
          </td>
        </tr>
      </table>
    </form>
  <?php
  }
}
//************************** EDIT REMEMBRANCE FORM - end

//************************** SHOW REMEMBRANCE
function show_remembrance($id, $fullshow)
{
  $table = 'remembrances';
  $r = get_record($table, $id);
  $eid = get_entry_id(get_table_id($table), $r->id);
  ?>
  <table>
    <tr>
      <td>
        <b><span style="background:#999999; color:#FFCCCC;">
        <a href="s_remembrances.php">خاطرات</a>
        </span></b>&nbsp;
        <a href="<? echo $main_url; ?>s_remembrances.php?id=<? 
          echo get_entry_id(get_table_id($table), $r->id); ?>">
        <b><? echo $r->title; ?></b></a>
      </td>
    </tr>
    <tr>
      <td>
        <br>
        <?
        $albumq = 'select id, pid from album a where a.pid='.$eid.
          ' and a.prim=1';
        $albumq = mysql_query($albumq);
        if (mysql_num_rows($albumq))
        {
          $albumq = mysql_fetch_object($albumq);
          echo '<img src="'.REMEMBRANCES_PIC_DIR.$eid.'-'.$albumq->id.'.jpg" align="left"'.
            wi_hi($fullshow).'>';
        }
        if ($fullshow)
        {
          echo get_personal_title($r->pid).'<br>';
          echo $r->text;
        }
        else
        {
          echo substr($r->text, 0, NSUMCHAR_BIG).set_etc($r->text, NSUMCHAR_BIG);
          echo '<br><a href="'.$main_url.'s_remembrances.php?id='.
            get_entry_id(get_table_id($table), $r->id).
            '">ادامه مطلب...</a>';
        }
        ?>
      </td>
    </tr>
  </table>
  <?
}
//************************** SHOW REMEMBRANCE - end

//************************** SHOW RECORD
function show_record($table, $id, $fullshow)
{
  switch ($table)
  {
    case 'martyrs':
      show_martyr($id, $fullshow);
      break;
    case 'news':
      show_news($id, $fullshow);
      break;
    case 'mtexts':
      show_mtext($id, $fullshow);
      break;
    case 'ltexts':
      show_ltext($id, $fullshow);
      break;
    case 'asar':
      show_asar($id, $fullshow);
      break;
    case 'remembrances':
      show_remembrance($id, $fullshow);
      break;
    default:
      echo '<table><tr><td align="center"><h2>خطا</h2></td></tr></table>';
      break;
  }
}
//************************** SHOW RECORD - end

//************************** NEW AUTHOR FORM
function new_author_form()
{
  global $PHP_SELF;
  if (is_this_manager())
  {
    ?>
    <form name="new_author" method="post" action="<? echo $PHP_SELF; ?>">
      <table class="tform" align="center">
        <tr>
          <td><label>نام:</label></td>
          <td><input type="text" name="author_name" maxlength="100"></td>
        </tr>
        <tr>
          <td><label>نام كاربري:</label></td>
          <td><input type="text" name="author_uname" maxlength="100"></td>
        </tr>
        <tr>
          <td><label>رمز عبور:</label></td>
          <td><input type="password" name="author_pass" maxlength="100"></td>
        </tr>
        <tr>
          <td><label>تأييد رمز عبور:</label></td>
          <td><input type="password" name="author_passc" maxlength="100"></td>
        </tr>
        <tr>
          <td colspan="2" align="center">
            <input type="submit" name="newauthor_submit" value="    ثبت   ">
          </td>
        </tr>
      </table>
    </form>
  <?
  }
  else
  {
    $mymsg = '<br><br>شما اجازه اين كار را نداريد.';
    redirect_rel('authors.php', $mymsg, 0);
  }
}
//************************** NEW AUTHOR FORM - end

//************************** EDIT AUTHOR FORM
function edit_author_form($e_author_id)
{
  global $PHP_SELF;
  $table = 'authors';
  $query = 'select * from '.$table.' a where '.
    'a.id='.$e_author_id;
  $e_authors =  mysql_query($query);
  $num = mysql_num_rows($e_authors);
  if ($num == 0)
    echo 'رکوردی با این شماره پیدا نشد.';
  else
  {
    $e_author1 = mysql_fetch_object($e_authors);
	if ($_SESSION['v_user'] == $e_author1->uname)
	{
      ?>
      <form method="post" action="<? echo $PHP_SELF; ?>?id=<?php echo $e_author_id; ?>">
        <table class="tform" align="center">
          <tr>
            <td><label>نام:</label></td>
            <td><input type="text" name="author_name" maxlength="100"
                value="<? echo $e_author1->name; ?>"></td>
          </tr>
          <tr>
            <td><label>نام كاربري:</label></td>
            <td><input type="text" name="author_uname" maxlength="100"
                value="<? echo $e_author1->uname; ?>"></td>
          </tr>
          <tr>
            <td><label>رمز عبور فعلي:</label></td>
            <td><input type="password" name="author_pass_now" maxlength="100"></td>
          </tr>
          <tr>
            <td><label>رمز عبور جديد:</label></td>
            <td><input type="password" name="author_pass_new" maxlength="100"></td>
          </tr>
          <tr>
            <td><label>تأييد رمز عبور جديد:</label></td>
            <td><input type="password" name="author_pass_new_c" maxlength="100"></td>
          </tr>
          <tr>
            <td colspan="2" align="center">
              <input type="submit" name="editauthor_submit" value="  ثبت تغییرات  ">
            </td>
          </tr>
        </table>
      </form>
      <?php
    }
    else
    {
      $mymsg = '<br><br>شما اجازه اين كار را نداريد.';
      redirect_rel('authors.php', $mymsg, 0);
    }
  }
}
//************************** EDIT AUTHOR FORM - end

//************************** DELETE AUTHOR FORM
function del_author_form($d_author_id)
{
  global $PHP_SELF;
  $q = 'select name, uname from authors a where a.id='.$d_author_id;
  $q = mysql_query($q);
  if (@mysql_num_rows($q))
  {
    if (is_this_manager())
    {
      $q = mysql_fetch_object($q);
      if ($_SESSION['v_user'] == $q->uname)
	  {
        $mymsg = '<br><br><b>شما اجازه اين كار را نداريد.</b>';
        redirect_rel('authors.php', $mymsg, 0);
	  }
      else
      {
        echo '<table class="tform" align="center" cellspacing="3">';
        echo '<tr><td>'.$q->name.'<br>'.$q->uname.'<br><br>';
        echo '<form action="'.$PHP_SELF.'?id='.$d_author_id.'" method="post">';
        echo 'رمز عبور مديريت:<br>'.
          '<input type="password" name="m_pass"><br>'.
          '<input type="submit" name="del_author_submit" value=" حذف ">';
        echo '</form></td></tr></table>';
      }
    }
    else
    {
      $mymsg = '<br><br><b>شما اجازه اين كار را نداريد.</b>';
      redirect_rel('authors.php', $mymsg, 0);
    }
  }
  else
  {
    $mymsg = '<br><br>چنين ركوردي پيدا نشد.<br>';
    redirect_rel('authors.php', $mymsg, 0);
  }
}
//************************** DELETE AUTHOR FORM - end

?>