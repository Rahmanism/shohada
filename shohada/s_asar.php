﻿<? @session_start(); ?>
<html dir="rtl">
<?php
require_once('inc.php');
html_top('آرشيو آثار', '', 'asar');
?>

<!------------------------------------------------>
<?php
$table = 'asar';
$id = $_REQUEST['id'];
$start_limit = $_REQUEST['start_limit'];
$arch = $_REQUEST['arch'];
if (!$start_limit)
  $start_limit = 0;

if ($id)
{
  ?>
  <table>
    <tr id="asar">
      <td style="border-bottom:#660002 solid 1px;">
        <?
        $tid = get_entry_tid($id);
        show_asar($tid->eid, 1);
        ?>&nbsp;<br>
      </td>
    </tr>
    <tr>
      <td align="center">
      <?
      $qn = 'select id from '.$table.' a where a.publish=1 '.
        'and id<'.$tid->eid.' order by id desc limit 1';
      $qn = mysql_query($qn);
      $qn = mysql_fetch_object($qn);
      if ($qn)
      {
        $qn = get_entry_id(get_table_id($table), $qn->id);
        echo '<a href="s_asar.php?arch=0&id='.$qn.'"><قبلي</a>';
      }
	  echo '&nbsp;&nbsp;|&nbsp;&nbsp;';
      $qn = 'select id from '.$table.' a where a.publish=1 '.
        'and id>'.$tid->eid.' limit 1';
      $qn = mysql_query($qn);
      $qn = mysql_fetch_object($qn);
      if ($qn)
      {
        $qn = get_entry_id(get_table_id($table), $qn->id);
        echo '<a href="s_asar.php?arch=0&id='.$qn.'">بعدي></a>';
      }
      ?>    
      </td>
    </tr>
  </table>
  <?
}
else
{
  if ($arch)
  {  /////////// SHOW Archive
    $lastnotes = 'select id, title from '.$table.' a'.
      ' where a.publish=1 order by id desc limit '.
      $start_limit.','.NOTES_IN_PAGE;
    $lastnotes = mysql_query($lastnotes);
    $nlastnotes = mysql_num_rows($lastnotes);
    $num_of_entries = get_num_of_records($table);
    $page_no = floor($num_of_entries / NOTES_IN_PAGE) + 1; // number of pages
    $current_page_no = (floor($start_limit / NOTES_IN_PAGE)) + 1;
    $next_limit = $start_limit + NOTES_IN_PAGE;
    $pre_limit = $start_limit - NOTES_IN_PAGE;
    if ($next_limit > $num_of_entries)
      $next_limit = ($page_no - 1) * NOTES_IN_PAGE;
    if ($pre_limit < 0)
      $pre_limit = 0;
    if ($num_of_entries > NOTES_IN_PAGE)
    {
      echo '<table align="center"><tr><td align="center">';
      echo '<a href="'.$PHP_SELF.'?arch=1&start_limit=0">';
      echo '<صفحه اول'.'</a>&nbsp;&nbsp;';
      echo '<a href="'.$PHP_SELF.'?arch=1&start_limit='.$pre_limit.'">';
      echo '<صفحه قبل'.'</a>';
      echo '<div style="width:50; position: absolute; ">';
      echo '<form name="page_go">';
      echo '<select name="page_num" onchange="jump_page('.NOTES_IN_PAGE.',\'s_asar.php\')">';
      for ($counter = 1; $counter <= $page_no; $counter++)
      {
        if ($counter == $current_page_no)
          $sel = ' selected';
        else
          $sel = '';
        echo '<option'.$sel.'>'.$counter.'</option>';
      }
      echo '</select>';
      echo '</form>';
      echo '</div>';
//      echo '-'.$current_page_no.'-&nbsp;&nbsp;';
      echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.
        '<a href="'.$PHP_SELF.'?arch=1&start_limit='.$next_limit.'">';
      echo 'صفحه بعد>'.'</a>&nbsp;&nbsp;';
      $last_page = ($page_no - 1) * NOTES_IN_PAGE;
      echo '<a href="'.$PHP_SELF.'?arch=1&start_limit='.$last_page.'">';
      echo 'صفحه آخر>'.'</a>&nbsp;&nbsp;';
      echo '</td></tr></table><hr>';
    }
    echo "<table>\n";
    for ($nl = 0; $nl < $nlastnotes; $nl++)
    {
      $alast = mysql_fetch_object($lastnotes);
      echo "<tr><td colspan=\"2\">\n";
      $eid = get_entry_id(get_table_id($table), $alast->id);
      echo '<b>&bull;</b> <a href="'.$PHP_SELF.'?id='.$eid.'">';
      echo untitled($alast->title).'</a>';
      echo "</td></tr>\n";
    }
    echo '</table>';
  }
  else
  { ////// SHOW Five records
    $asar = 'select id from '.$table.
      ' a where a.publish=1 order by id desc limit '.FIRST_NO;
    $asar = mysql_query($asar);
    $nasar = mysql_num_rows($asar);
    echo '<table>';
    for ($i = 0; $i < $nasar; $i++)
    {
      $asar1 = mysql_fetch_object($asar);
      echo '<tr><td style="border-bottom:#660002 solid 1px;">';
      show_asar($asar1->id, 0);
      echo '</td></tr>';
    }
    echo '</table>';
  }
}
html_bottom();
?>